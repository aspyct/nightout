﻿using System;
using System.Reactive;
using System.Reactive.Disposables;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Widget;
using Moonlight.Droid;
using Moonlight.Droid.Extensions;
using Moonlight.Droid.Services.Location;
using NightOut.Core.LocationSelector;
using NightOut.Droid.Home;

namespace NightOut.Droid.UI
{
    [Activity(
        ScreenOrientation = ScreenOrientation.Portrait,
        ParentActivity = typeof(HomeActivity),
        WindowSoftInputMode = SoftInput.AdjustResize
    )]
    internal class CheckInActivity : NightOutActivity<ICheckInPageViewModel>
    {
        private const int SettingsRequestCode = 42;

        private Android.Support.V7.Widget.Toolbar toolbar;
        private TextInputEditText nightName;
        private Button checkInButton;
        private Button refreshLocationButton;
        private Button openInMapsButton;
        private TextView latitudeLabelView;
        private TextView longitudeLabelView;
        private TextView accuracyLabelView;
        private TextView latitudeValueView;
        private TextView longitudeValueView;
        private TextView accuracyValueView;
        private ProgressBar progressBar;

        private FusedLocationService locationService;
        private IDisposable disposables;

        protected override ICheckInPageViewModel FindViewModel() => Dependencies.Instance.CheckInPageViewModel(this);

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            locationService = new FusedLocationService(this, SettingsRequestCode);

            AttachViews();
            SetupToolbar();

            latitudeLabelView.Text = ViewModel.LatitudeText;
            longitudeLabelView.Text = ViewModel.LongitudeText;
            accuracyLabelView.Text = ViewModel.AccuracyText;

            disposables = new CompositeDisposable(
                latitudeValueView.BindText(ViewModel.LatitudeValue),
                longitudeValueView.BindText(ViewModel.LongitudeValue),
                accuracyValueView.BindText(ViewModel.AccuracyValue),
                checkInButton.BindCommand(ViewModel.CheckInCommand),
                refreshLocationButton.BindCommand(ViewModel.RefreshLocationCommand),
                openInMapsButton.BindCommand(ViewModel.OpenInMapsCommand),
                nightName.BindCommand(ViewModel.UpdateNightNameCommand),
                ViewModel.RefreshLocationCommand.IsExecuting.Subscribe(ShowProgressBar)
            );
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Android.Content.Intent data)
        {
            switch (requestCode)
            {
                case SettingsRequestCode:
                    locationService.OnActivityResult(resultCode);
                    break;
                default:
                    base.OnActivityResult(requestCode, resultCode, data);
                    break;
            }
        }

        private void AttachViews()
        {
            SetContentView(Resource.Layout.CheckIn_Activity);
            this.FindView(out toolbar, Resource.Id.Toolbar);
            this.FindView(out nightName, Resource.Id.NightName);
            this.FindView(out checkInButton, Resource.Id.CheckInButton);
            this.FindView(out refreshLocationButton, Resource.Id.RefreshLocation);
            this.FindView(out openInMapsButton, Resource.Id.OpenInMaps);
            this.FindView(out longitudeLabelView, Resource.Id.LongitudeLabel);
            this.FindView(out latitudeLabelView, Resource.Id.LatitudeLabel);
            this.FindView(out accuracyLabelView, Resource.Id.AccuracyLabel);
            this.FindView(out longitudeValueView, Resource.Id.LongitudeValue);
            this.FindView(out latitudeValueView, Resource.Id.LatitudeValue);
            this.FindView(out accuracyValueView, Resource.Id.AccuracyValue);
            this.FindView(out progressBar, Resource.Id.ProgressBar);
        }

        private void SetupToolbar()
        {
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);
        }

        protected override void OnDestroy()
        {
            disposables.Dispose();

            base.OnDestroy();
        }

        private void ShowProgressBar(bool toggle)
        {
            progressBar.Visibility = toggle ? ViewStates.Visible : ViewStates.Gone;

            refreshLocationButton.Visibility = toggle ? ViewStates.Gone : ViewStates.Visible;
            openInMapsButton.Visibility = refreshLocationButton.Visibility;
        }
    }
}