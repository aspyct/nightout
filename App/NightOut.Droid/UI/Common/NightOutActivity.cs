﻿using System;
using Android.Support.Design.Widget;
using Moonlight;
using Moonlight.Droid;

namespace NightOut.Droid
{
    internal abstract class NightOutActivity<TViewModel> : BaseActivity<TViewModel>
         where TViewModel : class, IBaseViewModel
    {
        protected override void ShowTransientMessage(ITransientMessage message)
        {
            ContentView.ShowSnackbar(message);
        }
    }

    internal abstract class NightOutActivity<TViewModel, TParameter> : BaseActivity<TViewModel, TParameter>
        where TViewModel : class, IParameterizedViewModel<TParameter>
        where TParameter : class
    {
        protected override void ShowTransientMessage(ITransientMessage message)
        {
            ContentView.ShowSnackbar(message);
        }
    }
}
