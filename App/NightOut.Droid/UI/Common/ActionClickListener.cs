﻿using System;
using Android.Views;
using Android.Runtime;

namespace NightOut.Droid
{
    public class ActionClickListener : Java.Lang.Object, View.IOnClickListener
    {
        public Action ClickAction { get; set; }

        public void OnClick(View v)
        {
            ClickAction?.Invoke();
        }
    }
}
