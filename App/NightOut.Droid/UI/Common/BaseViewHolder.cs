﻿using System;
using Android.Support.V7.Widget;
using Android.Views;

namespace NightOut.Droid
{
    public abstract class BaseViewHolder : RecyclerView.ViewHolder
    {
        public BaseViewHolder(View itemView) : base(itemView)
        {
        }

        public abstract void Bind(object cellViewModel);

        public virtual void Recycle()
        {
            // Override me
        }
    }
}
