﻿using System;
using System.Reactive.Disposables;
using Android.Content.PM;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V7.Widget;
using Android.Widget;
using Moonlight.Droid;
using Moonlight.Droid.Extensions;
using Moonlight.Droid.Services.Location;
using NightOut.Core.Home;

namespace NightOut.Droid.Home
{
    [Android.App.Activity(
        Label = "Leaf a Note",
        ScreenOrientation = ScreenOrientation.Portrait,
        Name = "org.aspyct.leafanote.home",
        MainLauncher = true,
        Theme = "@style/AppTheme.Home",
        LaunchMode = LaunchMode.SingleTop
    )]
    internal class HomeActivity : NightOutActivity<IHomePageViewModel>
    {
        private RecyclerView recyclerView;
        private FloatingActionButton checkInButton;

        private RecyclerViewAdapter adapter;
        private IDisposable disposables;

        protected override IHomePageViewModel FindViewModel() => Dependencies.Instance.HomePageViewModel(this);

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Home_Activity);
            this.FindView(out recyclerView, Resource.Id.RecyclerView);
            this.FindView(out checkInButton, Resource.Id.CheckInButton);

            adapter = new RecyclerViewAdapter();
            recyclerView.SetAdapter(adapter);
            recyclerView.SetItemAnimator(new CheckInCellAnimator());

            disposables = new CompositeDisposable(
                adapter.Bind(ViewModel.Cells, ViewModel.CellUpdates),
                checkInButton.BindCommand(ViewModel.CheckInCommand)
            );

            Window.SetBackgroundDrawable(null);
        }

        protected override void OnDestroy()
        {
            disposables.Dispose();

            base.OnDestroy();
        }
    }
}