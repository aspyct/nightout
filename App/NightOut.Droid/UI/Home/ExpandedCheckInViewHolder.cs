﻿using System;
using Android.Support.V4.Content;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Views;
using Android.Widget;
using FFImageLoading;
using FFImageLoading.Views;
using Moonlight.Droid.Extensions;
using NightOut.Core.Home;
using System.Reactive.Disposables;
using Android.Support.V7.Widget;

namespace NightOut.Droid.Home
{
    public class ExpandedCheckInViewHolder : CheckInViewHolder
    {
        private readonly ImageViewAsync mapView;
        private readonly Toolbar toolbar;

        public ExpandedCheckInViewHolder(View itemView) : base(itemView)
        {
            this.mapView = itemView.FindViewById<ImageViewAsync>(Resource.Id.Map);
            this.toolbar = itemView.FindViewById<Toolbar>(Resource.Id.Toolbar);

            toolbar.InflateMenu(Resource.Menu.home_checkin_cell);
            toolbar.MenuItemClick += HandleMenuItemClick;
        }

        public override void Bind(object cellViewModel)
        {
            base.Bind(cellViewModel);

            var vm = (ICheckInCellViewModel)cellViewModel;

            if (mapView.IsLayoutRequested)
            {
                System.Diagnostics.Debug.WriteLine("MapView hasn't been laid out yet. Waiting...");
                mapView.LayoutChange += LoadMapLater;
            }
            else
            {
                LoadMap();
            }
        }

        public override void Recycle()
        {
            mapView.SetImageDrawable(null);

            base.Recycle();
        }

        void LoadMap()
        {
            System.Diagnostics.Debug.WriteLine("Reloading map");

            var scale = 2;
            var width = mapView.Width / scale;
            var height = mapView.Height / scale;
            var color = ContextCompat.GetColor(ItemView.Context, Resource.Color.accent) & 0xffffff;

            var imageUrl = ViewModel.GetMapImageUrl(width, height, scale, color);

            Dependencies.Instance.ImageService
                        .LoadUrl(imageUrl)
                        .Error(exc => System.Diagnostics.Debug.WriteLine(exc.ToString()))
                        .Into(mapView);
        }

        void LoadMapLater(object sender, View.LayoutChangeEventArgs e)
        {
            mapView.LayoutChange -= LoadMapLater;
            LoadMap();
        }

        void HandleMenuItemClick(object sender, Toolbar.MenuItemClickEventArgs e)
        {
            switch (e.Item.ItemId)
            {
                case Resource.Id.Delete:
                    ViewModel.DeleteAction();
                    break;
                case Resource.Id.Rename:
                    ViewModel.RenameAction();
                    break;
                case Resource.Id.Share:
                    ViewModel.ShareAction();
                    break;
                case Resource.Id.OpenInMaps:
                    ViewModel.OpenInMapsAction();
                    break;
            }
        }
    }
}
