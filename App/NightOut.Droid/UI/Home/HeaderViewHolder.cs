﻿using System;
using Android.Views;
using Android.Widget;
using NightOut.Core.Home;

namespace NightOut.Droid.Home
{
    public class HeaderViewHolder : BaseViewHolder
    {
        private TextView titleView;

        public HeaderViewHolder(View itemView) : base(itemView)
        {
            titleView = itemView.FindViewById<TextView>(Resource.Id.Title);
        }

        public override void Bind(object cellViewModel)
        {
            var vm = (IHeaderCellViewModel)cellViewModel;

            titleView.Text = vm.Title;
        }
    }
}
