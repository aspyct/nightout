﻿using System.Reactive;
using Android.Views;
using Android.Widget;
using Monad;
using Moonlight;
using Moonlight.Droid.Extensions;
using NightOut.Core.Home;

namespace NightOut.Droid.Home
{
    public class TipOfTheDayViewHolder : BaseViewHolder
    {
        private readonly TextView titleView;
        private readonly TextView bodyView;
        private readonly Button primaryActionButton;
        private readonly Button secondaryActionButton;

        public TipOfTheDayViewHolder(View itemView) : base(itemView)
        {
            titleView = itemView.FindViewById<TextView>(Resource.Id.Title);
            bodyView = itemView.FindViewById<TextView>(Resource.Id.Body);
            primaryActionButton = itemView.FindViewById<Button>(Resource.Id.PrimaryAction);
            secondaryActionButton = itemView.FindViewById<Button>(Resource.Id.SecondaryAction);
        }

        public override void Bind(object cellViewModel)
        {
            var vm = (ITipOfTheDayCellViewModel)cellViewModel;

            titleView.Text = vm.Title;
            bodyView.Text = vm.Body;
            MaybeBindButton(primaryActionButton, vm.PrimaryAction);
            MaybeBindButton(secondaryActionButton, vm.SecondaryAction);
        }

        private void MaybeBindButton(Button button, Maybe<ILabeledCommand<Unit>> command)
        {
            command.Do(just: cmd => BindButton(button, cmd),
                       nothing: () => HideButton(button));
        }

        private void BindButton(Button button, ILabeledCommand<Unit> command)
        {
            // TODO Dispose of this binding
            button.BindText(command.Label);
            button.Visibility = ViewStates.Visible;
        }

        private void HideButton(Button button)
        {
            button.Visibility = ViewStates.Gone;
        }
    }
}
