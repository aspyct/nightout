﻿using System;
using System.Collections.Generic;
using System.Reactive.Disposables;
using Android.Support.V7.Widget;
using Android.Views;
using NightOut.Core;
using NightOut.Core.Home;

namespace NightOut.Droid.Home
{
    public class RecyclerViewAdapter : RecyclerView.Adapter
    {
        private const int TipOfTheDayCell = Resource.Layout.Home_TipOfTheDayCell;
        private const int CheckInCell = Resource.Layout.Home_CheckInCell;
        private const int ExpandedCheckInCell = Resource.Layout.Home_CheckInCell_Expanded;
        private const int HeaderCell = Resource.Layout.Home_HeaderCell;

        private IReadOnlyList<ICellViewModel> cellList;

        public IDisposable Bind(IObservable<IReadOnlyList<ICellViewModel>> cells, IObservable<int> updates)
        {
            var cellSubscription = cells.Subscribe(list =>
            {
                this.cellList = list;
                NotifyDataSetChanged();
            });

            var updatesSubscription = updates.Subscribe(position =>
            {
                NotifyItemChanged(position);
            });

            return new CompositeDisposable(cellSubscription, updatesSubscription);
        }

        public override int ItemCount => cellList.Count;

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var viewModel = GetViewModelAt(position);
            var viewHolder = (BaseViewHolder)holder;

            viewHolder.Bind(viewModel);
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var itemView = LayoutInflater.From(parent.Context)
                                         .Inflate(viewType, parent, false);

            switch (viewType)
            {
                case TipOfTheDayCell: return new TipOfTheDayViewHolder(itemView);
                case CheckInCell: return new CheckInViewHolder(itemView);
                case ExpandedCheckInCell: return new ExpandedCheckInViewHolder(itemView);
                case HeaderCell: return new HeaderViewHolder(itemView);
                default: throw new InvalidOperationException("Unknown view type: " + viewType);
            }
        }

        public override int GetItemViewType(int position)
        {
            var vm = GetViewModelAt(position);

            var tipOfTheDay = vm as ITipOfTheDayCellViewModel;
            if (tipOfTheDay != null)
            {
                return TipOfTheDayCell;
            }

            var checkIn = vm as ICheckInCellViewModel;
            if (checkIn != null)
            {
                return checkIn.Expanded ? ExpandedCheckInCell : CheckInCell;
            }

            var header = vm as IHeaderCellViewModel;
            if (header != null)
            {
                return HeaderCell;
            }

            throw new InvalidOperationException("Can't handle cell viewmodel: " + vm);
        }

        public override void OnViewRecycled(Java.Lang.Object holder)
        {
            var viewHolder = (BaseViewHolder)holder;
            viewHolder.Recycle();
        }

        private ICellViewModel GetViewModelAt(int position)
        {
            return cellList[position];
        }
    }
}
