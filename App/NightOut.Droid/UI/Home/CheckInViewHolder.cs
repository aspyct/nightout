﻿using System;
using System.Reactive.Disposables;
using Android.Support.V4.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Views.Animations;
using Android.Widget;
using FFImageLoading;
using FFImageLoading.Views;
using NightOut.Core.Home;

namespace NightOut.Droid.Home
{
    public class CheckInViewHolder : BaseViewHolder
    {
        private readonly TextView dayView;
        private readonly TextView monthView;
        private readonly TextView titleView;
        private readonly ActionClickListener clickListener;

        protected ICheckInCellViewModel ViewModel { get; private set; }

        public CheckInViewHolder(View itemView) : base(itemView)
        {
            dayView = itemView.FindViewById<TextView>(Resource.Id.Day);
            monthView = itemView.FindViewById<TextView>(Resource.Id.Month);
            titleView = itemView.FindViewById<TextView>(Resource.Id.Title);
            clickListener = new ActionClickListener();

            itemView.SetOnClickListener(clickListener);
        }

        public override void Bind(object cellViewModel)
        {
            ViewModel = (ICheckInCellViewModel)cellViewModel;

            dayView.Text = ViewModel.Day;
            monthView.Text = ViewModel.Month;
            titleView.Text = ViewModel.Title;

            clickListener.ClickAction = ViewModel.SelectAction;
        }

        public override void Recycle()
        {
            clickListener.ClickAction = null;
            ViewModel = null;
        }
    }
}
