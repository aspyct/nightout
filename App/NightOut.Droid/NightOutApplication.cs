﻿using System;
using Android.App;
using Android.Runtime;
using Microsoft.Azure.Mobile;
using Microsoft.Azure.Mobile.Analytics;
using Microsoft.Azure.Mobile.Crashes;

namespace NightOut.Droid
{
    [Application]
    public class NightOutApplication : Application
    {
        public NightOutApplication(IntPtr handle, JniHandleOwnership transfer) : base(handle, transfer)
        {
            
        }

        public override void OnCreate()
        {
            base.OnCreate();

            InitializeAzureMobile();
        }

        private void InitializeAzureMobile()
        {
            MobileCenter.Start(
                "a0df7ac2-eac2-49cd-bdf8-3569355b8bb0",
                typeof(Analytics),
                typeof(Crashes)
            );
        }
    }
}
