﻿using System;
using Android.App;
using Android.Content;
using Moonlight.Droid.Services.ExternalApps;
using Moonlight.Droid.Services.Location;
using Moonlight.Services.ExternalApps;
using Moonlight.Services.Location;
using NightOut.Core;

namespace NightOut.Droid
{
    public class PlatformDependencies : IPlatformDependencies
    {
        private readonly Activity context;

        public PlatformDependencies(Activity context)
        {
            this.context = context;
        }

        public ILocationService LocationService => new FusedLocationService(context, 42);

        public IExternalAppsService ExternalAppsService => new ExternalAppsService(context);
    }
}
