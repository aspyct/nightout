﻿using System;
using Moonlight;
using Moonlight.Droid;
using NightOut.Core;
using NightOut.Core.Persistence;
using NightOut.Droid.Home;
using NightOut.Droid.UI;
using Android.App;
using Blink;
using FFImageLoading;

namespace NightOut.Droid
{
    public class Dependencies : NightOutCoreDependencies
    {
        private static readonly Lazy<Dependencies> instance = new Lazy<Dependencies>(() => new Dependencies());
        public static Dependencies Instance => instance.Value;

        public IImageService ImageService => imageService.Resolve();

        protected IDependency<INavigationService> navigationService;
        protected IDependency<ISQLiteConfiguration> sqliteConfiguration;
        protected IDependency<IImageService> imageService;

        protected override INavigationService NavigationService => navigationService.Resolve();
        protected override ISQLiteConfiguration SQLiteConfiguration => sqliteConfiguration.Resolve();

        public Dependencies()
        {
            navigationService = new SingletonDependency<INavigationService>(() => 
            {
                return new NavigationServiceBuilder(BaseActivity.ParameterExtra)
                    .AddPage(Page.HomePage, typeof(HomeActivity))
                    .AddPage(Page.CheckInPage, typeof(CheckInActivity))
                    .Build();
            });

            sqliteConfiguration = new SingletonDependency<ISQLiteConfiguration>(() => new SQLiteConfiguration());
            imageService = new SingletonDependency<IImageService>(() =>
            {
                var service = FFImageLoading.ImageService.Instance;

                service.Initialize(new FFImageLoading.Config.Configuration()
                {
                    FadeAnimationEnabled = true,
                    FadeAnimationForCachedImages = true,
                    FadeAnimationDuration = 100
                });

                return service;
            });
        }

        protected override IPlatformDependencies PlatformDependencies(object context)
        {
            return new PlatformDependencies((Activity)context);
        }
    }
}
