﻿using System;
using System.IO;
using NightOut.Core.Persistence;
using SQLite;

namespace NightOut.Droid
{
    public class SQLiteConfiguration : ISQLiteConfiguration
    {
        public SQLiteConnectionString ConnectionString { get; private set; }

        public SQLiteConfiguration()
        {
            var dbPath = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.Personal),
                "database.db3"
            );

            ConnectionString = new SQLiteConnectionString(dbPath, true);
        }
    }
}
