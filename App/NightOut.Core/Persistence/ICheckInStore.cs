using NightOut.Core.Data;
using System;
using Monad;
using System.Threading.Tasks;

namespace NightOut.Core.Persistence
{
    interface ICheckInStore
    {
        IObservable<Maybe<ICheckIn>> Insert(ICheckIn checkIn);
        IObservable<Maybe<ICheckIn>> Get(int id);
        IObservable<ICheckIn> FetchAllCheckIns();
        Task<bool> Delete(int checkInId);
    }
}
