﻿using System;
using SQLite;

namespace NightOut.Core.Persistence
{
    public interface ISQLiteConfiguration
    {
        SQLiteConnectionString ConnectionString { get; }
    }
}
