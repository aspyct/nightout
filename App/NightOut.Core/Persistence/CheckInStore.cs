﻿using System;
using System.Diagnostics;
using System.Reactive.Concurrency;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Monad;
using NightOut.Core.Data;
using NightOut.Core.Persistence;
using SQLite;

namespace NightOut.Core.Persistence
{
    class CheckInStore : ICheckInStore
    {
        private readonly IDependencies dependencies;

        private readonly IObservable<SQLiteConnection> connection;
        private readonly IObservable<SQLiteConnection> store;

        public CheckInStore(IDependencies dependencies)
        {
            this.dependencies = dependencies;

            connection = Observable.Create<SQLiteConnection>(observer =>
            {
                var database = NewConnection();

                observer.OnNext(database);
                observer.OnCompleted();

                return Disposable.Create(() =>
                {
                    Debug.WriteLine("CheckInStore: Closing database connection");
                    database.Close();
                });
            });

            store = connection.Do(db =>
            {
                Debug.WriteLine("CheckInStore: Creating CheckIn table");
                db.CreateTable<CheckIn>();
            });
        }

        public IObservable<Maybe<ICheckIn>> Get(int id)
        {
            return store.Do(_ => Debug.WriteLine($"CheckInStore: Getting object #{id}"))
                        .Select(db => db.Find<CheckIn>(id))
                        .Cast<ICheckIn>()
                        .Select(Maybe.Return)
                        .Replay(1).RefCount();
        }

        public IObservable<Maybe<ICheckIn>> Insert(ICheckIn checkIn)
        {
            return store.Do(_ => Debug.WriteLine($"Saving checkIn #{checkIn.Id}"))
                        .Select(db => db.Insert(checkIn) == 1 ? checkIn : null)
                        .Cast<ICheckIn>()
                        .Select(Maybe.Return)
                        .Do(maybe => maybe.Do(just: obj => Debug.WriteLine($"Created CheckIn #{obj.Id}"),
                                              nothing: () => Debug.WriteLine("Could not save CheckIn object")));
        }

        public IObservable<ICheckIn> FetchAllCheckIns()
        {
            return store.SelectMany(db => db.Query<CheckIn>("select * from CheckIn order by CreatedAt desc"))
                        .Do(checkIn => Debug.WriteLine($"Extrated CheckIn #{checkIn.Id} from database"));
        }

        public Task<bool> Delete(int checkInId)
        {
            using (var connection = NewConnection())
            {
                var rowsAffected = connection.Delete<CheckIn>(checkInId);
                
                if (rowsAffected == 1)
                {
                    return Task.FromResult(true);
                }
                else if (rowsAffected == 0)
                {
                    return Task.FromResult(false);
                }
                else
                {
                    throw new Exception($"Error deleting CheckIn #{checkInId}: {rowsAffected} rows affected.");
                }
            }
        }

        private SQLiteConnection NewConnection()
        {
            Debug.WriteLine("CheckInStore: Opening database connection");
            return new SQLiteConnection(dependencies.Configuration.ConnectionString.DatabasePath);
        }

        public interface IDependencies
        {
            ISQLiteConfiguration Configuration { get; }
        }
    }
}
