﻿using System;
using System.Collections.Generic;
using NightOut.Core.Data;
using NightOut.Core.Services.CheckIn;
using Vivaldi;

namespace NightOut.Core.Services.CheckIn
{
    public interface ICheckInSeason
    {
        Season Season { get; }
        IReadOnlyList<ICheckIn> CheckInList { get; }
        int Count { get; }
    }
}
