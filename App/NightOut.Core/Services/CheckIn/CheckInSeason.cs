﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reactive.Linq;
using Blink;
using NightOut.Core.Data;
using Vivaldi;

namespace NightOut.Core.Services.CheckIn
{
    public class CheckInSeason : ICheckInSeason
    {
        private readonly Season season;
        private readonly List<ICheckIn> checkInList;

        public CheckInSeason(Season season)
        {
            this.season = season;
            this.checkInList = new List<ICheckIn>();
        }

        public void AddCheckIn(ICheckIn checkIn)
        {
            checkInList.Add(checkIn);
        }

        public IReadOnlyList<ICheckIn> CheckInList => checkInList;
        public Season Season => season;
        public int Count => checkInList.Count;
    }
}
