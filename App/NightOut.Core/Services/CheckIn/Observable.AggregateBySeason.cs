﻿using System;
using System.Reactive;
using System.Threading.Tasks;
using Blink;
using NightOut.Core.Data;
using Vivaldi;

namespace NightOut.Core.Services.CheckIn
{
    public static class ObservableExtensions
    {
        public static IObservable<ICheckInSeason> AggregateBySeason(
            this IObservable<ICheckIn> source,
            ISeasonFactory seasonFactory
        )
        {
            return new Aggregator(source, seasonFactory);
        }

        private class Aggregator : ObservableBase<ICheckInSeason>
        {
            private readonly IObservable<ICheckIn> source;
            private readonly ISeasonFactory seasonFactory;

            public Aggregator(
                IObservable<ICheckIn> source,
                ISeasonFactory seasonFactory)
            {
                this.source = source;
                this.seasonFactory = seasonFactory;
            }

            protected override IDisposable SubscribeCore(IObserver<ICheckInSeason> observer)
            {
                return source.Subscribe(new Observer(observer, seasonFactory));
            }

            private class Observer : ObserverBase<ICheckIn>
            {
                private readonly IObserver<ICheckInSeason> observer;
                private readonly ISeasonFactory seasonFactory;

                private CheckInSeason currentSeason;

                public Observer(
                    IObserver<ICheckInSeason> observer,
                    ISeasonFactory seasonFactory)
                {
                    this.observer = observer;
                    this.seasonFactory = seasonFactory;

                    currentSeason = SeasonWithDate(DateTime.Now);
                }

                protected override void OnNextCore(ICheckIn value)
                {
                    if (currentSeason.Season.Contains(value.CreatedAt))
                    {
                        currentSeason.AddCheckIn(value);
                    }
                    else
                    {
                        YieldCurrentSeasonIfNotEmpty();

                        currentSeason = SeasonWithDate(value.CreatedAt);
                        currentSeason.AddCheckIn(value);
                    }
                }

                protected override void OnCompletedCore()
                {
                    YieldCurrentSeasonIfNotEmpty();
                    observer.OnCompleted();
                }

                protected override void OnErrorCore(Exception error)
                {
                    observer.OnError(error);
                }

                private CheckInSeason SeasonWithDate(DateTime date)
                {
                    return new CheckInSeason(seasonFactory.Create(date));
                }

                private void YieldCurrentSeasonIfNotEmpty()
                {
                    if (currentSeason.Count > 0)
                    {
                        observer.OnNext(currentSeason);
                    }
                }
            }
        }
    }
}
