﻿using NightOut.Core.Data;
using System;
using System.Collections.Generic;
using Monad;
using System.Threading.Tasks;

namespace NightOut.Core.Services.CheckIn
{
    public interface ICheckInService
    {
        IObservable<Maybe<ICheckIn>> Create(ICheckIn checkIn);
        IObservable<ICheckInSeason> FetchCompleteLog();

        Task<bool> Delete(int checkInId);
    }
}
