﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Blink;
using Monad;
using NightOut.Core.Data;
using NightOut.Core.Persistence;
using Vivaldi;

namespace NightOut.Core.Services.CheckIn
{
    class CheckInService : ICheckInService
    {
        private readonly IDependencies dependencies;

        public CheckInService(IDependencies dependencies)
        {
            this.dependencies = dependencies;
        }

        public IObservable<Maybe<ICheckIn>> Create(ICheckIn checkIn)
        {
            return dependencies.CheckInStore.Insert(checkIn);
        }

        public Task<bool> Delete(int checkInId)
        {
            return dependencies.CheckInStore.Delete(checkInId);
        }

        public IObservable<ICheckInSeason> FetchCompleteLog()
        {
            return dependencies.CheckInStore
                .FetchAllCheckIns()
                .AggregateBySeason(new SeasonFactory());
        }

        public interface IDependencies
        {
            ICheckInStore CheckInStore { get; }
        }
    }
}
