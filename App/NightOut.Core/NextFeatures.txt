﻿- Show location on map instead of text coordinates
- Show my checkins on a map
- Find photos related to a checkin
- Select friend email from contacts
- Send position with SMS instead of network
- Find position without GPS or network
- Get a notification when you lose GSM network
- Keep a SQLiteConnection open, and reuse it
- Display the date of the next new season (be accurate!)