﻿using System;
using Moonlight.Services.ExternalApps;
using Moonlight.Services.Location;
using NightOut.Core.Home;

namespace NightOut.Core
{
    public interface IPlatformDependencies
    {
        ILocationService LocationService { get; }
        IExternalAppsService ExternalAppsService { get; }
    }
}
