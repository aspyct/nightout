﻿using System;
using Blink;
using Moonlight;
using NightOut.Core.Home;
using NightOut.Core.LocationSelector;
using NightOut.Core.Persistence;
using NightOut.Core.Services.CheckIn;

namespace NightOut.Core
{
    public abstract class NightOutCoreDependencies : DependencyContainer,
	    HomePageViewModel.IDependencies,
	    CheckInPageViewModel.IDependencies,
        CheckInService.IDependencies,
        CheckInStore.IDependencies
    {
        protected IDependency<IApiKeys> apiKeys;

        protected IParameterizedDependency<IHomePageViewModel, IPlatformDependencies> homePageViewModel;
        INavigationService HomePageViewModel.IDependencies.NavigationService => NavigationService;
        IApiKeys HomePageViewModel.IDependencies.ApiKeys => apiKeys.Resolve();
        ICheckInService HomePageViewModel.IDependencies.CheckInService => checkInService.Resolve();

        protected IDependency<ICheckInService> checkInService;
        ICheckInStore CheckInService.IDependencies.CheckInStore => checkInStore.Resolve();

        protected IParameterizedDependency<ICheckInPageViewModel, IPlatformDependencies> checkInPageViewModel;
        INavigationService CheckInPageViewModel.IDependencies.NavigationService => NavigationService;
        ICheckInService CheckInPageViewModel.IDependencies.CheckInService => checkInService.Resolve();

        private IDependency<ICheckInStore> checkInStore;
        ISQLiteConfiguration CheckInStore.IDependencies.Configuration => SQLiteConfiguration;

        protected abstract INavigationService NavigationService { get; }
        protected abstract ISQLiteConfiguration SQLiteConfiguration { get; }
        protected abstract IPlatformDependencies PlatformDependencies(object context);

        public IHomePageViewModel HomePageViewModel(object context)
        {
            return homePageViewModel.Resolve(
                PlatformDependencies(context)
            );
        }

        public ICheckInPageViewModel CheckInPageViewModel(object context)
        {
            return checkInPageViewModel.Resolve(
                PlatformDependencies(context)
            );
        }

        public NightOutCoreDependencies()
        {
            // Reference cycles are not an issue, since
            // - singletons will live forever anyway
            // - and we don't keep a reference to non-singleton objects

            apiKeys = new SingletonDependency<IApiKeys>(() => new ApiKeys());

            homePageViewModel = new ParameterizedDependency<IHomePageViewModel, IPlatformDependencies>(
                platformDeps => new HomePageViewModel(this, platformDeps)
            );

            checkInPageViewModel = new ParameterizedDependency<ICheckInPageViewModel, IPlatformDependencies>(
                platformDeps => new CheckInPageViewModel(this, platformDeps)
            );

            checkInService = new SingletonDependency<ICheckInService>(() => new CheckInService(this));
            checkInStore = new SingletonDependency<ICheckInStore>(() => new CheckInStore(this));
        }
    }
}
