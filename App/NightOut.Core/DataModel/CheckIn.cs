﻿using System;
using SQLite;

namespace NightOut.Core.Data
{
    public class CheckIn : ICheckIn
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Indexed, NotNull]
        public DateTime CreatedAt { get; set; }

        [NotNull]
        public string Title { get; set; }

        [Indexed, NotNull]
        public double Latitude { get; set; }

        [Indexed, NotNull]
        public double Longitude { get; set; }

        [Indexed, NotNull]
        public double Accuracy { get; set; }

        public CheckIn()
        {
            
        }

        public CheckIn(ICheckIn copy)
        {
            this.Id = copy.Id;
            this.Latitude = copy.Latitude;
            this.Longitude = copy.Longitude;
            this.Accuracy = copy.Accuracy;
            this.Title = copy.Title;
            this.CreatedAt = copy.CreatedAt;
        }
    }
}
