﻿using System;

namespace NightOut.Core.Data
{
    public interface ICheckIn
    {
        int Id { get; }
        string Title { get; }
        DateTime CreatedAt { get; }
        double Latitude { get; }
        double Longitude { get; }
        double Accuracy { get; }
    }
}
