﻿using System;
using System.Reactive;
using Moonlight;

namespace NightOut.Core.TimeAndEmail
{
    public interface ITimeAndEmailPageViewModel
    {
        string ScreenTitle { get; }
        string CheckOutBeforeText { get; }
        string RecipientText { get; }
        string SendNowText { get; }
        string CheckInText { get; }

        IObservable<bool> SendNowValue { get; }
        IObservable<DateTime> CheckOutTime { get; }
        IObservable<string> RecipientValue { get; }

        IObservableCommand<Unit> ToggleSendNowCommand { get; }
        IObservableCommand<DateTime> UpdateCheckOutTimeCommand { get; }
        IObservableCommand<string> UpdateRecipientEmailCommand { get; }
        IObservableCommand<Unit> CheckInCommand { get; }
    }
}
