﻿using System;
using Monad;

namespace NightOut.Core.TimeAndEmail
{
    [Serializable]
    public class State
    {
        public readonly DateTime checkOutTime = DateTime.UtcNow.AddHours(12);
        public readonly string recipientEmail = string.Empty;
        public readonly bool sendEmailNow = false;
    }
}
