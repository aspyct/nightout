﻿using Moonlight;
using System;
using Monad;
using System.Reactive;
using Moonlight.Services.Location;

namespace NightOut.Core.LocationSelector
{
	public interface ICheckInPageViewModel : IBaseViewModel
    {
        string LatitudeText { get; }
        string LongitudeText { get; }
        string AccuracyText { get; }

        IObservable<string> NightName { get; }
        IObservable<Maybe<string>> LongitudeValue { get; }
        IObservable<Maybe<string>> LatitudeValue { get; }
        IObservable<Maybe<string>> AccuracyValue { get; }

        ILabeledCommand<string> UpdateNightNameCommand { get; }
        ILabeledCommand<Unit> RefreshLocationCommand { get; }
        ILabeledCommand<Unit> CheckInCommand { get; }
        ILabeledCommand<Unit> OpenInMapsCommand { get; }
    }
}