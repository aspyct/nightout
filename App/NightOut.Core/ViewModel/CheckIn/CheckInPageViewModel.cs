﻿using System;
using System.Diagnostics;
using System.Reactive;
using System.Reactive.Linq;
using System.Threading;
using Monad;
using Moonlight;
using Moonlight.Core;
using Moonlight.Services.Location;
using NightOut.Core.Data;
using NightOut.Core.Services.CheckIn;

namespace NightOut.Core.LocationSelector
{
    internal class CheckInPageViewModel : BaseViewModel<State>, ICheckInPageViewModel
    {
        private readonly IDependencies dependencies;
        private readonly IPlatformDependencies platformDependencies;

        private readonly IObservable<string> nightName;
        private readonly IObservable<Maybe<string>> latitudeValue;
        private readonly IObservable<Maybe<string>> longitudeValue;
        private readonly IObservable<Maybe<string>> accuracyValue;
        private readonly LabeledCommand<string> updateNightNameCommand;
        private readonly LabeledCommand<Unit> refreshLocationCommand;
        private readonly LabeledCommand<Unit> checkInCommand;
        private readonly LabeledCommand<Unit> openInMapsCommand;
        private readonly IObservableCommand<Unit> getInitialLocationCommand;

        public CheckInPageViewModel(
            IDependencies dependencies,
            IPlatformDependencies platformDependencies
        ) : base(dependencies.NavigationService)
        {
            this.dependencies = dependencies;
            this.platformDependencies = platformDependencies;

            nightName = State.Select(state => state.name);
            latitudeValue = State.Select(state => state.position.Select(pos => FormatLatitude(pos.Latitude)));
            longitudeValue = State.Select(state => state.position.Select(pos => FormatLongitude(pos.Longitude)));
            accuracyValue = State.Select(state => state.position.Select(pos => FormatAccuracy(pos.Accuracy)));

            getInitialLocationCommand = MakeCommand(GetInitialLocation);

            refreshLocationCommand = new LabeledCommand<Unit>(
                "Refresh",
                MakeCommandFromObservable<Unit>(RefreshLocation)
            );

            checkInCommand = new LabeledCommand<Unit>(
                "Check in",
                MakeCommand(CheckIn)
            );

            updateNightNameCommand = new LabeledCommand<string>(
                "Name this night",
                MakeCommand<string>(UpdateNightName)
            );

            openInMapsCommand = new LabeledCommand<Unit>(
                "Open in Maps",
                MakeCommand(
                    OpenInMaps,
                    State.Select(s => s.position.Case(_ => true, false))
                )
            );
        }

        public override void Start()
        {
            base.Start();

            platformDependencies.LocationService.OnUncertainLocationAvailability = HandleUncertainLocationAvailability;
            getInitialLocationCommand.Apply(Unit.Default);
        }

        public override void Stop()
        {
            platformDependencies.LocationService.OnUncertainLocationAvailability = null;

            base.Stop();
        }

        private string FormatLatitude(double latitude) => latitude.ToString();
        private string FormatLongitude(double longitude) => longitude.ToString();
        private string FormatAccuracy(double accuracy)
        {
            var rounded = (int)accuracy;
            return $"{rounded}m";
        }

        private void HandleUncertainLocationAvailability(UncertainLocationAvailability info)
        {
            ShowTransientMessage(new TransientMessage(
                UncertainLocationAvailabilityText,
                ShowSettingsHelp,
                ShowSettingsHelpText
            ));
        }

        private void ShowSettingsHelp()
        {
            ShowAlert(
                UncertainLocationAvailabilityText,
                "It looks like we might have trouble getting your location. " +
                "Try adjusting your settings (enable location, disable airplane mode).",
                "Ok"
            );
        }

        private State UpdateNightName(string name, State state)
        {
            Console.WriteLine(name);
            return state.Derive(name: name);
        }

        private void CheckIn(State state)
        {
            state.position.Do(position =>
            {
                var checkIn = new CheckIn
                {
                    Latitude = position.Latitude,
                    Longitude = position.Longitude,
                    Accuracy = position.Accuracy,
                    Title = state.name,
                    CreatedAt = DateTime.Now
                };

                dependencies.CheckInService.Create(checkIn).Subscribe(saved =>
                {
                    saved.Do(just: _ => NavigateBack(),
                             nothing: () => HandleSaveError());
                });
            });
        }

        private void HandleSaveError()
        {
            Debug.WriteLine("CheckInPageViewModel: Error saving the CheckIn");
            Debugger.Break();
        }

        private void GetInitialLocation(State state)
        {
            state.position.Do(just: _ => { /* no need to get a new location */ },
                              nothing: () => refreshLocationCommand.Apply(Unit.Default));
        }
        
        private IObservable<State> RefreshLocation(Unit _, State state)
        {
            // So, we want to wait for at most 5 location updates
            // As soon as we have something accurate enough, use it and stop
            // But if nothing is accurate enough, take the best one
            var position = platformDependencies.LocationService.GetCurrentPosition().Take(5);
            var atMost100m = position.Where(pos => pos.Accuracy < 100);
            var mostAccurate = position.Min(new Position.AccuracyComparer());

            return atMost100m.Merge(mostAccurate)
                             .Take(1)
                             .Select(pos => state.Derive(
                                 position: Maybe.Return(pos)
                             ))
                             .Catch(HandleLocationNotAvailable(state))
                             .ObserveOn(SynchronizationContext.Current);
        }

        private Func<LocationNotAvailableException, IObservable<State>> HandleLocationNotAvailable(State state)
        {
            return (LocationNotAvailableException e) =>
            {
                HandleUncertainLocationAvailability(null);
                return Observable.Return(state);
            };
        }

        private void OpenInMaps(State state)
        {
            state.position.Do(pos => platformDependencies.ExternalAppsService.OpenInMaps(
                pos.Latitude,
                pos.Longitude
            ));
        }

        public string LatitudeText => "Latitude";
        public string LongitudeText => "Longitude";
        public string AccuracyText => "Accuracy";
        public string UncertainLocationAvailabilityText => "Check your location settings";
        public string ShowSettingsHelpText => "What?";
        public IObservable<string> NightName => nightName;
        public IObservable<Maybe<string>> LongitudeValue => longitudeValue;
        public IObservable<Maybe<string>> LatitudeValue => latitudeValue;
        public IObservable<Maybe<string>> AccuracyValue => accuracyValue;
        public ILabeledCommand<string> UpdateNightNameCommand => updateNightNameCommand;
        public ILabeledCommand<Unit> RefreshLocationCommand => refreshLocationCommand;
        public ILabeledCommand<Unit> CheckInCommand => checkInCommand;
        public ILabeledCommand<Unit> OpenInMapsCommand => openInMapsCommand;

        public interface IDependencies
        {
            INavigationService NavigationService { get; }
            ICheckInService CheckInService { get; }
        }
    }
}
