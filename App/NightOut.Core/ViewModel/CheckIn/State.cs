﻿using System;
using Monad;
using Moonlight.Services.Location;

namespace NightOut.Core.LocationSelector
{
    [Serializable]
    partial class State
    {
        public readonly Maybe<Position> position = Nothing<Position>.Default;
        public readonly string name = "";
    }
}
