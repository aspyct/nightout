﻿using System;
using System.Reactive;
using NightOut.Core.Data;
using NightOut.Core.Services.CheckIn;
using Vivaldi;

namespace NightOut.Core.Home
{
    public static class ObservableExtensions
    {
        public static IObservable<ICellViewModel> ConvertToCells(
            this IObservable<ICheckInSeason> seasons,
            Func<int, ICheckInSeason, ICellViewModel> makeHeader,
            Func<int, ICheckIn, ICellViewModel> makeCell
        )
        {
            return new Converter(seasons, makeHeader, makeCell);
        }

        private class Converter : ObservableBase<ICellViewModel>
        {
            private readonly IObservable<ICheckInSeason> source;
            private readonly Func<int, ICheckInSeason, ICellViewModel> makeHeader;
            private readonly Func<int, ICheckIn, ICellViewModel> makeCell;

            public Converter(
                IObservable<ICheckInSeason> source,
                Func<int, ICheckInSeason, ICellViewModel> makeHeader,
                Func<int, ICheckIn, ICellViewModel> makeCell
            )
            {
                this.source = source;
                this.makeHeader = makeHeader;
                this.makeCell = makeCell;
            }

            protected override IDisposable SubscribeCore(IObserver<ICellViewModel> observer)
            {
                return source.Subscribe(new Observer(observer, makeHeader, makeCell));
            }

            private class Observer : ObserverBase<ICheckInSeason>
            {
                private readonly IObserver<ICellViewModel> observer;
                private readonly Func<int, ICheckInSeason, ICellViewModel> makeHeader;
                private readonly Func<int, ICheckIn, ICellViewModel> makeCell;

                private int position;

                public Observer(
                    IObserver<ICellViewModel> observer,
                    Func<int, ICheckInSeason, ICellViewModel> makeHeader,
                    Func<int, ICheckIn, ICellViewModel> makeCell
                )
                {
                    this.observer = observer;
                    this.makeHeader = makeHeader;
                    this.makeCell = makeCell;

                    position = 0;
                }

                protected override void OnNextCore(ICheckInSeason value)
                {
                    observer.OnNext(makeHeader(position, value));
                    position += 1;

                    foreach (var checkIn in value.CheckInList)
                    {
                        observer.OnNext(makeCell(position, checkIn));
                        position += 1;
                    }
                }

                protected override void OnCompletedCore()
                {
                    observer.OnCompleted();
                }

                protected override void OnErrorCore(Exception error)
                {
                    observer.OnError(error);
                }
            }
        }
    }
}
