﻿using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.Reactive;
using System.Reactive.Subjects;
using Monad;
using Moonlight;
using NightOut.Core.Data;

namespace NightOut.Core.Home
{
    internal class CheckInCellViewModel : ICheckInCellViewModel
    {
        private readonly ICheckIn checkIn;
        private readonly int position;
        private readonly string day;
        private readonly string month;
        private readonly string title;
        private readonly double latitude;
        private readonly double longitude;

        public ICheckIn CheckIn => checkIn;
        public int Position => position;
        public string Day => day;
        public string Month => month;
        public string Title => title;
        public bool Expanded { get; set; }

        public string GoogleMapsApiKey { get; set; }
        public Action SelectAction { get; set; }
        public Action DeleteAction { get; set; }
        public Action RenameAction { get; set; }
        public Action OpenInMapsAction { get; set; }
        public Action ShareAction { get; set; }

        public CheckInCellViewModel(int position, ICheckIn checkIn)
        {
            this.position = position;
            this.checkIn = checkIn;

            day = FormatDay(checkIn);
            month = FormatMonth(checkIn);
            latitude = checkIn.Latitude;
            longitude = checkIn.Longitude;

            if (HasTitle(checkIn))
            {
                title = FormatTitle(checkIn);
            }
            else
            {
                title = FormatCoordinates(checkIn);
            }
        }

        public string GetMapImageUrl(int width, int height, int scale, int markerColor)
        {
            var lat = latitude.ToString();
            var lon = longitude.ToString();
            var zoom = 16;
            var color = "0x" + markerColor.ToString("x6");

            return $"https://maps.googleapis.com/maps/api/staticmap?size={width}x{height}&scale={scale}&zoom={zoom}&center={latitude},{longitude}&maptype=terrain&markers=color:{color}|{latitude},{longitude}&key={GoogleMapsApiKey}";
        }

        bool HasTitle(ICheckIn checkIn)
        {
            return !string.IsNullOrEmpty(checkIn.Title);
        }

        string FormatDay(ICheckIn checkIn)
        {
            return checkIn.CreatedAt.Day.ToString();
        }

        string FormatTitle(ICheckIn checkIn)
        {
            return checkIn.Title;
        }

        string FormatCoordinates(ICheckIn checkIn)
        {
            return $"{checkIn.Latitude}, {checkIn.Longitude}";
        }

        string FormatMonth(ICheckIn checkIn)
        {
            return checkIn.CreatedAt.ToString("MMM", CultureInfo.InvariantCulture);
        }
    }
}
