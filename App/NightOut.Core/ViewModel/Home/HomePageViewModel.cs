﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using Blink;
using Moonlight;
using Moonlight.Core;
using Moonlight.Services.ExternalApps;
using Moonlight.Services.Location;
using NightOut.Core.Data;
using NightOut.Core.Services.CheckIn;
using Vivaldi;

namespace NightOut.Core.Home
{
    internal class HomePageViewModel : BaseViewModel<State>, IHomePageViewModel
    {
        private const int NoSelectedCell = -1;

        private readonly IDependencies dependencies;
        private readonly IPlatformDependencies platformDependencies;

        private readonly ISubject<IReadOnlyList<ICellViewModel>> cells;
        private readonly ISubject<int> cellUpdates;
        private readonly IObservableCommand<CheckInCellViewModel> selectCheckInCommand;
        private readonly IObservableCommand<CheckInCellViewModel> deleteCheckInCommand;
        private readonly IObservableCommand<CheckInCellViewModel> renameCheckInCommand;
        private readonly IObservableCommand<ICheckIn> shareCheckInCommand;
        private readonly IObservableCommand<Unit> refreshCommand;
        private readonly IObservableCommand<Unit> checkInCommand;

        private CheckInCellViewModel selectedCheckInCell;

        public IObservable<IReadOnlyList<ICellViewModel>> Cells => cells;
        public IObservable<int> CellUpdates => cellUpdates;
        public IObservableCommand<Unit> RefreshCommand => refreshCommand;
        public IObservableCommand<Unit> CheckInCommand => checkInCommand;

        public HomePageViewModel(
            IDependencies dependencies,
            IPlatformDependencies platformDependencies
        ) : base(dependencies.NavigationService)
        {
            this.dependencies = dependencies;
            this.platformDependencies = platformDependencies;

            cells = new BehaviorSubject<IReadOnlyList<ICellViewModel>>(new List<ICellViewModel>(capacity: 0));
            cellUpdates = new Subject<int>();

            selectCheckInCommand = MakeCommand<CheckInCellViewModel>(SelectCheckIn);
            deleteCheckInCommand = MakeCommand<CheckInCellViewModel>(DeleteCheckIn);
            renameCheckInCommand = MakeCommand<CheckInCellViewModel>(RenameCheckIn);
            shareCheckInCommand = MakeCommand<ICheckIn>(ShareCheckIn);
            refreshCommand = MakeCommand(Refresh);
            checkInCommand = MakeCommand(CheckIn);
        }

        public override void Start()
        {
            base.Start();

            refreshCommand.Apply(Unit.Default);
        }

        private void Refresh()
        {
            Debug.WriteLine("HomePageViewModel: Refreshing");

            var service = dependencies.CheckInService;
            service.FetchCompleteLog()
                   .ConvertToCells(MakeHeader, MakeCell)
                   .ToList()
                   .Select(list => new ReadOnlyCollection<ICellViewModel>(list))
                   .Subscribe(
                       list => cells.OnNext(list),
                       exc => Debug.WriteLine(exc.ToString())
                   );
        }

        private ICellViewModel MakeHeader(int position, ICheckInSeason season)
        {
            return new HeaderCellViewModel(position) {
                Title = SeasonToString(season)
            };
        }

        private ICellViewModel MakeCell(int position, ICheckIn checkIn)
        {
            var vm = new CheckInCellViewModel(position, checkIn);

            vm.OpenInMapsAction = () => OpenInMaps(checkIn);
            vm.GoogleMapsApiKey = dependencies.ApiKeys.GoogleMaps;
            // TODO Check: does that cause a memory leak?
            // Normally the generational GC should handle reference cycles
            vm.SelectAction = () => selectCheckInCommand.Apply(vm);
            vm.DeleteAction = () => deleteCheckInCommand.Apply(vm);
            vm.RenameAction = () => renameCheckInCommand.Apply(vm);
            vm.ShareAction = () => shareCheckInCommand.Apply(checkIn);

            return vm;
        }

        private string SeasonToString(ICheckInSeason checkInSeason)
        {
            var season = checkInSeason.Season;
            var which = season.Which;
            var startYear = season.Start.Year;
            var endYear = season.End.Year;

            switch (which)
            {
                case Season.Name.Spring:
                    return $"Spring {startYear}";
                case Season.Name.Summer:
                    return $"Summer {startYear}";
                case Season.Name.Autumn:
                    return $"Autumn {startYear}";
                case Season.Name.Winter:
                    return $"Winter {startYear} - {endYear}";
                default:
                    throw new InvalidOperationException($"Unknown season: {season}");
            }
        }

        private void CheckIn(State state)
        {
            NavigateTo(Page.CheckInPage);
        }

        private void OpenInMaps(ICheckIn checkIn)
        {
            var result = platformDependencies.ExternalAppsService.OpenInMaps(
                checkIn.Latitude,
                checkIn.Longitude
            );

            if (!result)
            {
                ShowTransientMessage(new TransientMessage("Could not open Maps app"));
            }
        }

        private void SelectCheckIn(CheckInCellViewModel cell)
        {
            if (cell != selectedCheckInCell)
            {
                // Select another cell
                // Collapse the currently open one,
                // and expand the new one
                if (selectedCheckInCell != null)
                {
                    selectedCheckInCell.Expanded = false;
                    cellUpdates.OnNext(selectedCheckInCell.Position);
                }

                cell.Expanded = true;
                cellUpdates.OnNext(cell.Position);

                selectedCheckInCell = cell;
            }
            else
            {
                // Selected the open cell.
                // Collapse it
                selectedCheckInCell.Expanded = false;
                cellUpdates.OnNext(selectedCheckInCell.Position);

                selectedCheckInCell = null;
            }
        }

        private void DeleteCheckIn(CheckInCellViewModel cell)
        {
            ShowAlert(
                $"Delete {cell.Title}?",
                "The check-in will be lost forever.",
                new LabeledAction("Delete", () => ActuallyDeleteCheckIn(cell.CheckIn)),
                new LabeledAction("Cancel")
            );
        }

        private void ActuallyDeleteCheckIn(ICheckIn checkIn)
        {
            Debug.WriteLine($"CheckInService: {dependencies.CheckInService}");
            var service = dependencies.CheckInService;

            service.Delete(checkIn.Id).ContinueWith(task =>
            {
                var success = task.Result;

                if (success)
                {
                    ShowTransientMessage(new TransientMessage("Check-in deleted"));
                    refreshCommand.Apply(Unit.Default);
                }
                else
                {
                    ShowAlert("Error", "Could not delete the checkin, Please try again later.", "ok");
                }
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        private void RenameCheckIn(CheckInCellViewModel cell)
        {
            ShowTransientMessage(new TransientMessage("Not yet implemented."));
        }

        private void ShareCheckIn(ICheckIn checkIn)
        {
            platformDependencies.ExternalAppsService.Share(
                "Share this checkin",
                $"Latitude: {checkIn.Latitude}, Longitude: {checkIn.Longitude}"
            );
        }

        public interface IDependencies
        {
            INavigationService NavigationService { get; }
            IApiKeys ApiKeys { get; }
            ICheckInService CheckInService { get; }
        }
    }
}
