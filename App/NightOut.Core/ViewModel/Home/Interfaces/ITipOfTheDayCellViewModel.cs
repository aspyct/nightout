﻿using System;
using System.Reactive;
using Monad;
using Moonlight;

namespace NightOut.Core.Home
{
    public interface ITipOfTheDayCellViewModel : ICellViewModel
    {
        string Title { get; }
        string Body { get; }
        Maybe<ILabeledCommand<Unit>> PrimaryAction { get; }
        Maybe<ILabeledCommand<Unit>> SecondaryAction { get; }
    }
}
