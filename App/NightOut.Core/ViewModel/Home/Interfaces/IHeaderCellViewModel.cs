﻿using System;
namespace NightOut.Core.Home
{
    public interface IHeaderCellViewModel : ICellViewModel
    {
        string Title { get; }
    }
}
