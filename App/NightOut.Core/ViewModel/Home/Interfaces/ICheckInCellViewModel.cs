﻿using System;
using System.Reactive;
using Monad;
using Moonlight;

namespace NightOut.Core.Home
{
    public interface ICheckInCellViewModel : ICellViewModel
    {
        string Title { get; }
        string Day { get; }
        string Month { get; }
        bool Expanded { get; }

        Action SelectAction { get; }
        Action DeleteAction { get; }
        Action RenameAction { get; }
        Action OpenInMapsAction { get; }
        Action ShareAction { get; }

        string GetMapImageUrl(int width, int height, int scale, int markerColor);
    }
}
