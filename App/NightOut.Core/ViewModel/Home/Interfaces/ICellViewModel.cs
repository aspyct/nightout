﻿using System;
namespace NightOut.Core.Home
{
    public interface ICellViewModel
    {
        int Position { get; }
    }
}
