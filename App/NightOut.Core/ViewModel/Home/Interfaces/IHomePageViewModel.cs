﻿using Moonlight;
using System;
using Monad;
using System.Reactive;
using System.Collections.Generic;
using Moonlight.Services.Location;

namespace NightOut.Core.Home
{
    public interface IHomePageViewModel : IBaseViewModel
    {
        IObservable<IReadOnlyList<ICellViewModel>> Cells { get; }
        IObservable<int> CellUpdates { get; }
        IObservableCommand<Unit> CheckInCommand { get; }
    }
}
