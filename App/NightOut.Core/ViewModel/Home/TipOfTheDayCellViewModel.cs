﻿using System;
using System.Reactive;
using Monad;
using Moonlight;

namespace NightOut.Core.Home
{
    public class TipOfTheDayCellViewModel : ITipOfTheDayCellViewModel
    {
        private readonly int position;

        public TipOfTheDayCellViewModel(int position)
        {
            this.position = position;
        }

        public int Position => position;
        public string Title { get; set; }
        public string Body { get; set; }
        public Maybe<ILabeledCommand<Unit>> PrimaryAction { get; set; } = Nothing<ILabeledCommand<Unit>>.Default;
        public Maybe<ILabeledCommand<Unit>> SecondaryAction { get; set; } = Nothing<ILabeledCommand<Unit>>.Default;
    }
}
