﻿using System;

namespace NightOut.Core.Home
{
    public class HeaderCellViewModel : IHeaderCellViewModel
    {
        private readonly int position;

        public HeaderCellViewModel(int position)
        {
            this.position = position;
        }

        public int Position => position;
        public string Title { get; set; }
    }
}
