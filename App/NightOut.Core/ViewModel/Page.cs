﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NightOut.Core
{
    public static class Page
    {
        public const string HomePage = nameof(HomePage);
        public const string CheckInPage = nameof(CheckInPage);
        public const string CheckInDetailsPage = nameof(CheckInDetailsPage);
        public const string TimeAndEmailPage = nameof(TimeAndEmailPage);
    }
}
