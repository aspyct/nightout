﻿using System;
namespace Blink
{
    public class SingletonDependency<TProduct> : BaseDependency<TProduct>, IDependency<TProduct>
    {
        private readonly Lazy<TProduct> instance;
        
        public SingletonDependency(TProduct instance) : this(() => instance) {}

        public SingletonDependency(Func<TProduct> factory)
        {
            instance = new Lazy<TProduct>(() => Verify(factory()));
        }

        public TProduct Resolve()
        {
            return instance.Value;
        }
    }
}
