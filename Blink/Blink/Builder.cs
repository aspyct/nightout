﻿using System;
namespace Blink
{
    public class Builder<TProduct>
    {
        private readonly Func<TProduct> factory;

        public TProduct Value => factory();

        public Builder(Func<TProduct> factory)
        {
            this.factory = factory;
        }
    }
}
