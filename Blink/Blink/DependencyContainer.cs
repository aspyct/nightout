﻿using System;
namespace Blink
{
    public class DependencyContainer
    {
        public DependencyContainer()
        {
        }

        protected void Preload<TProduct>(IDependency<TProduct> dependency)
        {
            dependency.Resolve();
        }
    }
}
