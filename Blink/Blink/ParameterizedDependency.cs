﻿using System;
namespace Blink
{
    public class ParameterizedDependency<TProduct, TParameter> : BaseDependency<TProduct>, IParameterizedDependency<TProduct, TParameter>
    {
        private readonly Func<TParameter, TProduct> factory;


        public ParameterizedDependency(Func<TParameter, TProduct> factory)
        {
            this.factory = factory;
        }

        public TProduct Resolve(TParameter parameter) => Verify(factory(parameter));
    }
}
