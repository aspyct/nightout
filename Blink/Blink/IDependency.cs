﻿using System;
namespace Blink
{
    public interface IDependency<TProduct>
    {
        TProduct Resolve();
    }
}
