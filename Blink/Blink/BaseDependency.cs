﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blink
{
    public abstract class BaseDependency<TProduct>
    {
        private readonly bool notNull;
        private readonly string name;

        public BaseDependency(bool notNull = true, string name = null)
        {
            this.notNull = notNull;
            this.name = name;
        }

        protected TProduct Verify(TProduct product)
        {
            if (product == null && notNull)
            {
                throw new InvalidOperationException($"Dependency product is null but cannot be null: {Name}");
            }

            return product;
        }

        private string Name
        {
            get
            {
                if (name != null)
                {
                    return name;
                }
                else
                {
                    return GetType().GetGenericTypeDefinition().GenericTypeArguments[0].FullName;
                }
            }
        }
    }
}
