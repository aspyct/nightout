﻿using System;
namespace Blink
{
    public interface IParameterizedDependency<TProduct, TParameter>
    {
        TProduct Resolve(TParameter parameter);
    }
}
