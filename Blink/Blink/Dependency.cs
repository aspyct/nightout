﻿using System;
namespace Blink
{
    public class Dependency<TProduct> : BaseDependency<TProduct>, IDependency<TProduct>
    {
        private readonly Func<TProduct> factory;

        public Dependency(Func<TProduct> factory)
        {
            this.factory = factory;
        }

        public TProduct Resolve() => Verify(factory());
    }
}
