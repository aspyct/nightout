﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moonlight
{
    public interface INavigationService
    {
        Task NavigateTo(object context, string page, object argument);
        Task NavigateBack(object context);

        Task OpenModal(object context, string page, object argument);
        Task DismissModal(object context);

        void ShowAlert(object context, string title, string body, string ok);
        void Confirm(object context, string title, string body, ILabeledAction ok, ILabeledAction cancel);
    }
}
