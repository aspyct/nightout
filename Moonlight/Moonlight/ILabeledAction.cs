using System;
namespace Moonlight
{
    public interface ILabeledAction
    {
        string Label { get; }
        Action Execute { get; }
    }
}
