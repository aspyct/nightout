﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moonlight
{
    public interface IParameterizedViewModel<TParameter> : IBaseViewModel
        where TParameter : class
    {
        TParameter Parameter { get; set; }
    }
}
