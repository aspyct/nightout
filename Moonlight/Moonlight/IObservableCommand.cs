﻿using System;
using System.Reactive;

namespace Moonlight
{
    public interface IObservableCommand<TArg>
    {
        IDisposable Apply(TArg arg);
        IObservable<bool> CanExecute { get; }
        IObservable<bool> IsExecuting { get; }
    }
}
