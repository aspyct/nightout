﻿using System;
using System.Reactive;

namespace Moonlight
{
    public interface IBaseViewModel
    {
        object NavigationContext { get; set; }
        Action<ITransientMessage> ShowTransientMessage { get; set; }

        void Start();
        void Stop();

		object DumpState();
		void RestoreState(object state);
    }
}
