﻿using System;

namespace Moonlight.Services.Location
{
    public interface ILocationService
    {
        Action<UncertainLocationAvailability> OnUncertainLocationAvailability { get; set; }
        IObservable<Position> GetCurrentPosition();
    }
}
