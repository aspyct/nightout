﻿using System;
using System.Collections.Generic;

namespace Moonlight.Services.Location
{
    [Serializable]
    public class Position
    {
        public readonly double Latitude;
        public readonly double Longitude;
        public readonly double Accuracy;

        public Position(double latitude, double longitude, double accuracy)
        {
            Latitude = latitude;
            Longitude = longitude;
            Accuracy = accuracy;
        }

        public class AccuracyComparer : IComparer<Position>
        {
            public int Compare(Position x, Position y)
            {
                return (int)(x.Accuracy - y.Accuracy);
            }
        }

        public override string ToString()
        {
            return string.Format($"[Position(lat:{Latitude}, lon:{Longitude}, acc:{Accuracy})]");
        }
    }
}
