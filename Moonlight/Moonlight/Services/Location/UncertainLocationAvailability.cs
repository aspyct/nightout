namespace Moonlight.Services.Location
{
    public class UncertainLocationAvailability
    {
        public bool NetworkEnabled { get; set; }
        public bool GpsEnabled { get; set; }
    }
}