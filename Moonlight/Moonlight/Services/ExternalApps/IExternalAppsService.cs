﻿using System;

namespace Moonlight.Services.ExternalApps
{
    public interface IExternalAppsService
    {
        bool OpenInMaps(double latitude, double longitude);
        bool Share(string title, string sharedtext);
    }
}
