﻿using System;
namespace Moonlight
{
    public interface ILabeledCommand<TArg> : IObservableCommand<TArg>
    {
        IObservable<string> Label { get; }
    }
}
