using System;

namespace Moonlight
{
    public interface ITransientMessage
    {
        string Text { get; }
        bool HasAction { get; }
        string ActionText { get; }
        Action Action { get; }
    }
}