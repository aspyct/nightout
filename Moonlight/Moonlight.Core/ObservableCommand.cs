﻿using Moonlight;
using System;
using System.Collections.Generic;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Reactive.Disposables;
using System.Diagnostics;

namespace Moonlight.Core
{
    public class ObservableCommand<TArg, TState> : IObservableCommand<TArg>
    {
        private readonly BehaviorSubject<bool> isExecuting;
        private readonly IObservable<bool> canExecute;
        private readonly ISubject<TState> state;
        private readonly Func<TArg, TState, IObservable<TState>> command;

        public IObservable<bool> CanExecute => canExecute;
        public IObservable<bool> IsExecuting => isExecuting;

        public ObservableCommand(
            ISubject<TState> state,
            Func<TArg, TState, IObservable<TState>> command,
            IObservable<bool> canExecute = null
        )
        {
            this.canExecute = canExecute ?? Observable.Return(true);
            this.isExecuting = new BehaviorSubject<bool>(false);
            this.command = command;
            this.state = state;
        }

        public IDisposable Apply(TArg arg)
        {
            var execution = state
                .Take(1)
                // Notify that we are executing
                .Do(_ => isExecuting.OnNext(true))

                // Execute the actual command
                .Select(instant => command(arg, instant))
                .Switch()

                .Subscribe(
                    instant => state.OnNext(instant), // Update the state
                    HandleException, // Show error
                    () => isExecuting.OnNext(false) // Notify that we're done running
                );

            return Disposable.Create(() =>
            {
                execution.Dispose();
                isExecuting.OnNext(false);
            });
        }

        private void HandleException(Exception e)
        {
            Debug.WriteLine(e.ToString());
            Debugger.Break();
        }

        private enum ExecutionState
        {

        }
    }
}
