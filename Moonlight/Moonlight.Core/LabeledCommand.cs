﻿using System;
using System.Reactive;
using System.Reactive.Subjects;

namespace Moonlight.Core
{
    public static class LabeledCommand
    {
        public static ILabeledCommand<TArg> Create<TArg>(string label, IObservableCommand<TArg> command)
        {
            return new LabeledCommand<TArg>(label, command);
        }
    }

    public class LabeledCommand<TArg> : ILabeledCommand<TArg>
    {
        private readonly BehaviorSubject<string> label;
        private readonly IObservableCommand<TArg> command;

        public LabeledCommand(string label, IObservableCommand<TArg> command)
        {
            this.label = new BehaviorSubject<string>(label);
            this.command = command;
        }

        public void UpdateLabel(string label)
        {
            this.label.OnNext(label);
        }

        public IObservable<string> Label => label;
        public IObservable<bool> CanExecute => command.CanExecute;
        public IObservable<bool> IsExecuting => command.IsExecuting;
        public IDisposable Apply(TArg arg) => command.Apply(arg);
    }
}
