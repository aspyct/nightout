﻿using Monad;
using System;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;

namespace Moonlight.Core
{
    public abstract class BaseViewModel<TState> : IBaseViewModel
        where TState : class, new()
    {
        public object NavigationContext { get; set; }
        public Action<ITransientMessage> ShowTransientMessage { get; set; }

        private BehaviorSubject<TState> state;
        protected IObservable<TState> State => state;

        private readonly INavigationService navigation;

        protected BaseViewModel(INavigationService navigation)
        {
            this.navigation = navigation;
            this.state = new BehaviorSubject<TState>(new TState());
        }

        protected void NavigateTo(string page, object parameter = null)
        {
            navigation.NavigateTo(NavigationContext, page, parameter);
        }

        protected void NavigateBack()
        {
            navigation.NavigateBack(NavigationContext);
        }

        protected void ShowAlert(string title, string body, string ok)
        {
            navigation.ShowAlert(NavigationContext, title, body, ok);
        }

        protected void ShowAlert(string title, string body, ILabeledAction ok, ILabeledAction cancel)
        {
            navigation.Confirm(NavigationContext, title, body, ok, cancel);
        }

        public virtual void Start()
        {
            // Override me
        }

        public virtual void Stop()
        {
            // Override me
        }

        public object DumpState()
        {
            return state.Value;
        }

        public void RestoreState(object state)
        {
            this.state.OnNext((TState)state);
        }

        protected IObservableCommand<Unit> MakeCommandFromAsync(Func<Task> command, IObservable<bool> canExecute = null)
        {
            return MakeCommand(
                () => command().ConfigureAwait(false),
                canExecute
            );
        }

        protected IObservableCommand<Unit> MakeCommand(Action command, IObservable<bool> canExecute = null)
        {
            return MakeCommand<Unit>((_, state) =>
            {
                command();
                return state;
            }, canExecute);
        }

        protected IObservableCommand<TArg> MakeCommand<TArg>(Action<TArg> command, IObservable<bool> canExecute = null)
        {
            return MakeCommand<TArg>((arg, state) =>
            {
                command(arg);
                return state;
            }, canExecute);
        }

        protected IObservableCommand<Unit> MakeCommand(Action<TState> command, IObservable<bool> canExecute = null)
        {
            return MakeCommand<Unit>((_, state) =>
            {
                command(state);
                return state;
            }, canExecute);
        }

        protected IObservableCommand<TArg> MakeCommand<TArg>(
            Func<TArg, TState, TState> command,
            IObservable<bool> canExecute = null
        )
        {
            return MakeCommandFromObservable<TArg>(
                (arg, state) => Observable.Return(command(arg, state)),
                canExecute
            );
        }
        
        protected IObservableCommand<TArg> MakeCommandFromObservable<TArg>(
            Func<TArg, TState, IObservable<TState>> command,
            IObservable<bool> canExecute = null)
        {
            return new ObservableCommand<TArg, TState>(state, command, canExecute);
        }
    }
}