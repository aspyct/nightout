using System;
namespace Moonlight.Core
{
    public class LabeledAction : ILabeledAction
    {
        private string label;
        private Action execute;

        public LabeledAction(string label) : this(label, () => { })
        {

        }

        public LabeledAction(string label, Action execute)
        {
            this.label = label;
            this.execute = execute;
        }

        public string Label => label;
        public Action Execute => execute;
    }
}
