﻿using System;

namespace Moonlight.Core
{
    public class TransientMessage : ITransientMessage
    {
        private readonly string text;
        private readonly Action action;
        private readonly string actionText;

        public TransientMessage(string text, Action action = null, string actionText = null)
        {
            this.text = text;
            this.action = action;
            this.actionText = actionText;
        }

        public string Text => text;
        public bool HasAction => action != null;
        public Action Action => action;
        public string ActionText => actionText;
    }
}
