﻿namespace Moonlight.Core
{
    public class BaseParameterizedViewModel<TState, TParameter> : BaseViewModel<TState>, IParameterizedViewModel<TParameter>
		where TState: class, new()
        where TParameter: class
    {
        public TParameter Parameter { get; set; }

        public BaseParameterizedViewModel(INavigationService navigation) : base(navigation)
        {

        }
    }
}
