﻿using System;
using System.Reactive.Disposables;
using Android.Runtime;
using Android.Text;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;

namespace Moonlight.Droid
{
    public static class EditTextExtensions
    {
        public static IDisposable BindCommand(
            this EditText self,
            ILabeledCommand<string> command
        )
        {
            return new CompositeDisposable(
                command.Label.Subscribe(label => self.Hint = label),
                new Wiring(self, command)
            );
        }

        class Wiring : IDisposable
        {
            private readonly EditText edit;
            private readonly IObservableCommand<string> command;

            public Wiring(EditText edit, IObservableCommand<string> command)
            {
                this.edit = edit;
                this.command = command;

                this.edit.TextChanged += TextChanged;
            }

            public void Dispose()
            {
                this.edit.TextChanged -= TextChanged;
            }

            void TextChanged(object sender, TextChangedEventArgs e)
            {
                command.Apply(edit.Text);
            }
        }
    }
}
