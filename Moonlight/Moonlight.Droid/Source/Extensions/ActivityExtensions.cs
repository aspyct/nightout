﻿using System.Diagnostics.Contracts;
using Android.App;
using Android.Views;

namespace Moonlight.Droid.Extensions
{
    public static class ActivityExtensions
    {
        public static void FindView<TView>(this Activity self, out TView view, int id)
            where TView : View
        {
            Contract.Requires(view != null);
            Contract.Requires(self != null);

            view = self.FindViewById<TView>(id);
        }
    }
}
