﻿using System;

namespace Moonlight.Droid.Extensions
{
    public interface IButtonAdapter
    {
        event EventHandler Click;
        bool Enabled { get; set; }
    }
}
