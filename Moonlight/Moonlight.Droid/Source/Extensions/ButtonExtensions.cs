using System;
using System.Diagnostics.Contracts;
using System.Reactive.Disposables;
using Android.Widget;

namespace Moonlight.Droid.Extensions
{
    public static class ButtonExtensions
    {
        public static IDisposable BindCommand(
            this Button button,
            ILabeledAction command
        )
        {
            Contract.Requires(button != null);
            Contract.Requires(command != null);

            button.Text = command.Label;
            return new CommandButtonWiring(new ButtonAdapter(button), command);
        }

        public static IDisposable BindCommand<TArg>(
            this Button button,
            IObservableCommand<TArg> command,
            TArg argument = default(TArg))
        {
            Contract.Requires(button != null);
            Contract.Requires(command != null);
            Contract.Ensures(Contract.Result<IDisposable>() != null);

            return new ObservableCommandButtonWiring<TArg>(new ButtonAdapter(button), command, argument);
        }

        public static IDisposable BindCommand<TArg>(
            this Button button,
            ILabeledCommand<TArg> command,
            TArg argument = default(TArg)
        )
        {
            Contract.Requires(command != null);
            Contract.Requires(button != null);
            Contract.Ensures(Contract.Result<IDisposable>() != null);

            return new CompositeDisposable(
                button.BindText(command.Label),
                button.BindCommand(
                    command as IObservableCommand<TArg>,
                    argument
                )
            );
        }

        public static IDisposable BindCommand<TArg>(
            this ImageButton button,
            IObservableCommand<TArg> command,
            TArg argument = default(TArg)
        )
        {
            Contract.Requires(button != null);
            Contract.Requires(command != null);
            Contract.Ensures(Contract.Result<IDisposable>() != null);

            return new ObservableCommandButtonWiring<TArg>(new ImageButtonAdapter(button), command, argument);
        }
    }
}
