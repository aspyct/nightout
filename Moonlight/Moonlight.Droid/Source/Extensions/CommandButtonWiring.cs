using System;
namespace Moonlight.Droid.Extensions
{
    public class CommandButtonWiring : IDisposable
    {
        private readonly IButtonAdapter button;
        private readonly ILabeledAction command;

        public CommandButtonWiring(IButtonAdapter button, ILabeledAction command)
        {
            this.button = button;
            this.command = command;

            button.Click += HandleClick;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                button.Click -= HandleClick;
            }
        }

        void HandleClick(object sender, EventArgs e)
        {
            command.Execute();
        }
   }
}
