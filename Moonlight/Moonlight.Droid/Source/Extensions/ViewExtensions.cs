﻿using System;
using Android.Support.Design.Widget;
using Android.Views;

namespace Moonlight.Droid
{
    public static class ViewExtensions
    {
        public static void ShowSnackbar(this View self, ITransientMessage message)
        {
            var length = message.HasAction ? Snackbar.LengthLong : Snackbar.LengthShort;
            var bar = Snackbar.Make(self, message.Text, length);

            if (message.HasAction)
            {
                bar.SetAction(message.ActionText, _ => message.Action());
            }

            bar.Show();
        }
    }
}
