﻿using System;
using Android.Widget;

namespace Moonlight.Droid.Extensions
{
    public class ButtonAdapter : IButtonAdapter
    {
        private readonly Button button;

        public ButtonAdapter(Button button)
        {
            this.button = button;
        }

        public bool Enabled
        {
            get { return button.Enabled; }
            set { button.Enabled = value; }
        }

        public event EventHandler Click
        {
            add { button.Click += value; }
            remove { button.Click -= value; }
        }
    }
}
