using Android.Widget;
using Monad;
using System;
using System.Linq;
using System.Reactive.Linq;
using System.Diagnostics.Contracts;

namespace Moonlight.Droid.Extensions
{
    public static class TextViewExtensions
    {
        public static IDisposable BindText<TValue>(this TextView self, IObservable<TValue> observable, Func<TValue, string> converter = null)
        {
            return BindText(self, observable.Select(Maybe.Return));
        }

        public static IDisposable BindText<TValue>(this TextView self, IObservable<Maybe<TValue>> observable, Func<TValue, string> converter = null)
        {
            Contract.Requires(observable != null);
            Contract.Ensures(Contract.Result<IDisposable>() != null);
            Contract.Requires(self != null);

            return observable
                .Select(maybe => maybe
                    .Select(converter != null ? converter
                                              : val => val.ToString())
                    .Or("")
                )
                .Subscribe(str => self.Text = str);
        }
    }
}
