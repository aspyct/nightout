﻿using System;
using Android.Widget;

namespace Moonlight.Droid.Extensions
{
    public class ImageButtonAdapter : IButtonAdapter
    {
        private readonly ImageButton button;

        public ImageButtonAdapter(ImageButton button)
        {
            this.button = button;
        }

        public bool Enabled
        {
            get { return button.Enabled; }
            set { button.Enabled = value; }
        }

        public event EventHandler Click
        {
            add { button.Click += value; }
            remove { button.Click -= value; }
        }
    }
}
