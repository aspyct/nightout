﻿using System;
using System.Reactive.Linq;
using System.Reactive.Disposables;
using System.Diagnostics.Contracts;

namespace Moonlight.Droid.Extensions
{
    class ObservableCommandButtonWiring<TArg> : IDisposable
    {
        private readonly IButtonAdapter button;
        private readonly IObservableCommand<TArg> command;
        private readonly TArg argument;
        private readonly IDisposable enabledBinding;

        private IDisposable currentExecution = Disposable.Empty;

        public ObservableCommandButtonWiring(IButtonAdapter button, IObservableCommand<TArg> command, TArg argument)
        {
            Contract.Requires(button != null);
            Contract.Requires(command != null);

            this.button = button;
            this.command = command;
            this.argument = argument;

            button.Click += HandleClick;
            enabledBinding = command.CanExecute.Subscribe(canExecute => button.Enabled = canExecute);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                button.Click -= HandleClick;
                enabledBinding.Dispose();
                currentExecution.Dispose();
            }
        }

        private void HandleClick(object sender, EventArgs e)
        {
            currentExecution = command.Apply(argument);
        }
    }
}