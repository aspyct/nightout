﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;

using Microsoft.Practices.ServiceLocation;
using System.Threading.Tasks;
using System.Reactive;
using System.Diagnostics;
using Monad;
using Moonlight.Droid.Extensions;

namespace Moonlight.Droid
{
    public abstract class BaseActivity : AppCompatActivity
    {
        public const string ParameterExtra = "ParameterExtra";

        private readonly Lazy<View> contentView;
        protected View ContentView => contentView.Value;

        protected BaseActivity()
        {
            contentView = new Lazy<View>(FindContentView);
        }

        private View FindContentView() => FindViewById<View>(global::Android.Resource.Id.Content);
    }

    public abstract partial class BaseActivity<TViewModel> : BaseActivity
        where TViewModel : class, IBaseViewModel
    {
        private const string ViewModelStateKey = "ViewModelState";

        public TViewModel ViewModel { get; set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            ViewModel = FindViewModel();

            ViewModel.NavigationContext = this;
            ViewModel.ShowTransientMessage = ShowTransientMessage;
        }

        protected abstract TViewModel FindViewModel();

        protected override void OnStart()
        {
            base.OnStart();

            ViewModel.Start();
        }

        protected override void OnStop()
        {
            base.OnStop();

            ViewModel.Stop();
        }

        protected override void OnSaveInstanceState(Bundle outState)
        {
            base.OnSaveInstanceState(outState);

            var state = ViewModel.DumpState();
            outState.PutObject(ViewModelStateKey, state);
        }

        protected override void OnRestoreInstanceState(Bundle savedInstanceState)
        {
            var state = savedInstanceState.GetObject(ViewModelStateKey);
            ViewModel.RestoreState(state);

            base.OnRestoreInstanceState(savedInstanceState);
        }

        protected abstract void ShowTransientMessage(ITransientMessage message);
    }

    public abstract class BaseActivity<TViewModel, TParameter> : BaseActivity<TViewModel>
        where TViewModel : class, IParameterizedViewModel<TParameter>
        where TParameter : class
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            //ViewModel.Parameter = Intent.GetObjectExtra<TParameter>(ParameterExtra);
        }
    }
}