﻿using System;
using Android.Gms.Location;
using Moonlight.Services.Location;

namespace Moonlight.Droid.Services.Location
{
    class LocationCatcher : IDisposable
    {
        private readonly LocationListener listener = new LocationListener();
        private readonly IObserver<Position> observer;

        public LocationCatcher(IObserver<Position> observer)
        {
            this.observer = observer;
            this.listener.LocationChanged += LocationChanged;
        }

        public LocationListener Listener => listener;

        public void Dispose()
        {
            listener.LocationChanged -= LocationChanged;
        }

        private void LocationChanged(Android.Locations.Location location)
        {
            observer.OnNext(new Position(
                location.Latitude,
                location.Longitude,
                location.Accuracy
            ));
        }
    }
}
