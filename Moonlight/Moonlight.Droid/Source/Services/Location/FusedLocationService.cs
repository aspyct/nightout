﻿using System;
using System.Diagnostics;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Gms.Common.Apis;
using Android.Gms.Location;
using Android.Locations;
using Moonlight.Droid.Services.GoogleApi;
using Moonlight.Services.Location;

namespace Moonlight.Droid.Services.Location
{
    public class FusedLocationService : ILocationService
    {
        private readonly Activity activity;
        private readonly LocationRequest locationRequest;
        private readonly int settingsRequestCode;
        private readonly GoogleApiConnector connector;

        private Action<Result> activityResultHandler;

        public Action<UncertainLocationAvailability> OnUncertainLocationAvailability { get; set; }

        public FusedLocationService(Activity activity, int settingsRequestCode)
        {
            this.activity = activity;
            this.settingsRequestCode = settingsRequestCode;
            this.locationRequest = MakeLocationRequest();
            this.connector = new GoogleApiConnector(activity, LocationServices.API);
        }

        public IObservable<Position> GetCurrentPosition()
        {
            return connector.Client.Value.ToObservable()
                            .Do(conn => Debug.WriteLine($"Got connection: {conn}"))
                            .Do(_ => Debug.WriteLine("Verifying location settings"))
                            .Select(VerifyLocationSettings)
                            .Switch()
                            .Do(_ => Debug.WriteLine("Good. Getting location"))
                            .SelectMany(CreateLocationObserver)
                            .Do(pos => Debug.WriteLine($"Got position: {pos}"))
                            .Publish().RefCount();
        }


        public void OnActivityResult(Result resultCode)
        {
            activityResultHandler?.Invoke(resultCode);
            activityResultHandler = null;
        }

        private LocationRequest MakeLocationRequest()
        {
            var request = new LocationRequest();

            request.SetPriority(100); // High accuracy
            request.SetInterval(2000); // Refresh every 2s

            return request;
        }

        private IObservable<GoogleApiClient> VerifyLocationSettings(GoogleApiClient client)
        {
            var settingsRequest = new LocationSettingsRequest.Builder()
                                                             .SetAlwaysShow(true)
                                                             .AddLocationRequest(locationRequest)
                                                             .Build();

            return Observable.StartAsync(() => LocationServices.SettingsApi.CheckLocationSettingsAsync(
                                 client, settingsRequest
                             ))
                             .Select(result => HandleServiceStatus(client, result.Status))
                             .Switch();
        }

        private IObservable<GoogleApiClient> HandleServiceStatus(GoogleApiClient client, Statuses status)
        {
            switch (status.StatusCode)
            {
                case CommonStatusCodes.Success: return Observable.Return(client);
                case CommonStatusCodes.ResolutionRequired: return WaitForUserAction(client, status);
                case LocationSettingsStatusCodes.SettingsChangeUnavailable: return ManualLocationCheck(client);
                default: throw new InvalidOperationException($"Cannot handle status: {status}");
            }
        }

        private IObservable<GoogleApiClient> WaitForUserAction(GoogleApiClient client, Statuses status)
        {
            var tcs = new TaskCompletionSource<GoogleApiClient>();

            activityResultHandler?.Invoke(Result.Canceled);
            activityResultHandler = resultCode =>
            {
                if (resultCode == Result.Ok)
                {
                    tcs.TrySetResult(client);
                }
                else
                {
                    tcs.TrySetException(new LocationNotAvailableException());
                }
            };

            status.StartResolutionForResult(activity, settingsRequestCode);

            return tcs.Task.ToObservable();
        }

        private IObservable<GoogleApiClient> ManualLocationCheck(GoogleApiClient client)
        {
            Debug.WriteLine("LocationService: Google can't tell us how to get location. Checking for ourselves");

            /*
            It looks like we can get a "Settings change unavailable" when the user
            is in airplane mode, but the location is enabled.
            So let's check whether location is enabled, because that's all we need.
            https://github.com/googlesamples/android-play-location/issues/47
            */
            var locationManager = (LocationManager)activity.GetSystemService(Context.LocationService);

            bool gpsEnabled;
            bool networkEnabled;

            try
            {
                Debug.WriteLine("LocationService: Checking for GPS support");
                gpsEnabled = locationManager.IsProviderEnabled(LocationManager.GpsProvider);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                gpsEnabled = false;
            }

            try
            {
                Debug.WriteLine("LocationService: Checking for network support");
                networkEnabled = locationManager.IsProviderEnabled(LocationManager.NetworkProvider);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                networkEnabled = false;
            }

            Debug.WriteLine($"LocationService: GPS: {gpsEnabled}, Network: {networkEnabled}");

            if (gpsEnabled || networkEnabled)
            {
                // Looks like we'll be able to get a location fix after all...
                OnUncertainLocationAvailability?.Invoke(new UncertainLocationAvailability
                {
                    GpsEnabled = gpsEnabled,
                    NetworkEnabled = networkEnabled
                });
                return Observable.Return(client);
            }
            else
            {
                // Nah, nothing we can do except ask the user to fix his settings
                throw new LocationNotAvailableException();
            }
        }

        private IObservable<Position> CreateLocationObserver(GoogleApiClient client)
        {
            return Observable.Create((IObserver<Position> observer) =>
            {
                var catcher = new LocationCatcher(observer);
                Debug.WriteLine("Starting location updates");
                LocationServices.FusedLocationApi.RequestLocationUpdates(
                    client,
                    locationRequest,
                    catcher.Listener,
                    activity.MainLooper
                );

                return Disposable.Create(() =>
                {
                    Debug.WriteLine("Stopping location updates");
                    if (client.IsConnected)
                    {
                        LocationServices.FusedLocationApi.RemoveLocationUpdates(client, catcher.Listener);
                    }
                    catcher.Dispose();
                });
            });
        }
    }
}
