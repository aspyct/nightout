﻿using System;
using System.Diagnostics;
using Android.Content;
using Moonlight.Services.ExternalApps;

namespace Moonlight.Droid.Services.ExternalApps
{
    public class ExternalAppsService : IExternalAppsService
    {
        private readonly Context context;

        public ExternalAppsService(Context context)
        {
            this.context = context;
        }

        public bool OpenInMaps(double latitude, double longitude)
        {
            var uri = Android.Net.Uri.Parse($"geo:{latitude},{longitude}");
            var intent = new Intent(Intent.ActionView, uri);

            return StartActivity(intent);
        }

        public bool Share(string title, string sharedText)
        {
            Intent intent = new Intent();
            intent.SetAction(Intent.ActionSend);
            intent.PutExtra(Intent.ExtraText, sharedText);
            intent.SetType("text/plain");

            return StartActivity(
                Intent.CreateChooser(intent, title)
            );
        }

        private bool StartActivity(Intent intent)
        {
            try
            {
                context.StartActivity(intent);
                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return false;
            }
        }
    }
}
