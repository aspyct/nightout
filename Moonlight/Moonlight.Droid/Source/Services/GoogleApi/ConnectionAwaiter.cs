﻿using System;
using System.Threading.Tasks;
using Android.Gms.Common;
using Android.Gms.Common.Apis;
using Android.OS;

namespace Moonlight.Droid.Services.GoogleApi
{
    class ConnectionAwaiter : Java.Lang.Object, GoogleApiClient.IConnectionCallbacks, GoogleApiClient.IOnConnectionFailedListener
    {
        private readonly GoogleApiClient client;
        private TaskCompletionSource<GoogleApiClient> tcs;

        public ConnectionAwaiter(GoogleApiClient client)
        {
            this.client = client;

            client.RegisterConnectionCallbacks(this);
            client.RegisterConnectionFailedListener(this);
        }

        public void Connect(TaskCompletionSource<GoogleApiClient> tcs)
        {
            this.tcs = tcs;
            client.Connect();
        }

        public void OnConnected(Bundle connectionHint)
        {
            tcs.TrySetResult(client);
        }

        public void OnConnectionSuspended(int cause)
        {
            // Nothing to do
        }

        public void OnConnectionFailed(ConnectionResult result)
        {
            tcs.TrySetException(
                new Exception("Google API client disconnected unexpectedly")
            );
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                client.UnregisterConnectionCallbacks(this);
                client.UnregisterConnectionFailedListener(this);
            }

            base.Dispose(disposing);
        }
    }
}
