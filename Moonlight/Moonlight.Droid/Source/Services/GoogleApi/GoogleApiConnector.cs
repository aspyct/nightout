﻿using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Android.Content;
using Android.Gms.Common;
using Android.Gms.Common.Apis;
using Android.OS;

namespace Moonlight.Droid.Services.GoogleApi
{
    public class GoogleApiConnector : IDisposable
    {
        private readonly GoogleApiClient client;
        private readonly Lazy<Task<GoogleApiClient>> connection;
        private readonly ConnectionAwaiter awaiter;

        public Lazy<Task<GoogleApiClient>> Client => connection;

        public GoogleApiConnector(Context context, params Api[] apiList)
        {
            var builder = new GoogleApiClient.Builder(context);

            foreach (var api in apiList)
            {
                builder.AddApi(api);
            }

            client = builder.Build();
            awaiter = new ConnectionAwaiter(client);
            connection = new Lazy<Task<GoogleApiClient>>(Connect);
        }

        private Task<GoogleApiClient> Connect()
        {
            var tcs = new TaskCompletionSource<GoogleApiClient>();

            awaiter.Connect(tcs);

            return tcs.Task;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                awaiter.Dispose();
                client.Disconnect();
                client.Dispose();
            }
        }
    }
}
