using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using Android.Content;

namespace Moonlight.Droid
{
    internal class NavigationService : INavigationService
    {
        public readonly Dictionary<string, Type> activityMap = new Dictionary<string, Type>();
        private readonly string parameterExtraKey;

        public NavigationService(string parameterExtraKey)
        {
            this.parameterExtraKey = parameterExtraKey;
        }

        public Task DismissModal(object context)
        {
            throw new NotImplementedException();
        }

        public Task NavigateTo(object context, string page, object argument)
        {
            var activity = (Activity)context;
            var activityType = GetActivityTypeFor(page);
            var intent = MakeIntent(activityType, argument, activity);

            activity.StartActivity(intent);

            return Task.FromResult(true);
        }

        public Task NavigateBack(object context)
        {
            var activity = (Activity)context;

            activity.Finish();

            return Task.FromResult(true);
        }

        private Intent MakeIntent(Type activityType, object argument, Activity activity)
        {
            var intent = new Intent(activity, activityType);

            if (argument != null)
            {
                intent.PutExtra(parameterExtraKey, argument);
            }

            return intent;
        }

        private Type GetActivityTypeFor(string page)
        {
            return activityMap[page];
        }

        public Task OpenModal(object context, string page, object argument)
        {
            throw new NotImplementedException();
        }

        public void ShowAlert(object context, string title, string body, string ok)
        {
            new AlertDialog((Context)context, title, body, ok).Show();
        }

        public void Confirm(object context, string title, string body, ILabeledAction ok, ILabeledAction cancel)
        {
            new ConfirmDialog((Context)context, title, body, ok, cancel).Show();
        }

        private class AlertDialog : Java.Lang.Object, IDialogInterfaceOnClickListener
        {
            private readonly Android.App.AlertDialog dialog;

            public AlertDialog(Context context, string title, string body, string ok)
            {
                dialog = new Android.App.AlertDialog.Builder(context)
                                        .SetTitle(title)
                                        .SetMessage(body)
                                        .SetNeutralButton(ok, this)
                                        .Create();
            }

            public void Show()
            {
                dialog.Show();
            }

            public void OnClick(IDialogInterface dialog, int which)
            {
                dialog.Dismiss();
            }
        }

        private class ConfirmDialog
        {
            private readonly Android.App.AlertDialog dialog;

            public ConfirmDialog(Context context, string title, string body, ILabeledAction ok, ILabeledAction cancel)
            {
                dialog = new Android.App.AlertDialog.Builder(context)
                                        .SetTitle(title)
                                        .SetMessage(body)
                                        .SetPositiveButton(ok.Label, delegate { ok.Execute(); })
                                        .SetNegativeButton(cancel.Label, delegate { cancel.Execute(); })
                                        .Create();
            }

            public void Show()
            {
                dialog.Show();
            }
        }
    }
}