using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Moonlight.Droid
{
    public static class SerializationExtensions
    {
        public static TParameter GetObjectExtra<TParameter>(this Intent intent, string name)
            where TParameter : class, new()
        {
            if (intent == null)
            {
                return default(TParameter);
            }

            var bytes = intent.GetByteArrayExtra(name);
			return Deserialize<TParameter>(bytes);
        }

        public static void PutExtra(this Intent intent, string name, object extra)
        {
			var bytes = Serialize(extra);
            intent.PutExtra(name, bytes);
        }

		public static void PutObject(this Bundle bundle, string key, object obj)
		{
			var bytes = Serialize(obj);
			bundle.PutByteArray(key, bytes);
		}

		public static object GetObject(this Bundle bundle, string key)
		{
			return GetObject<object>(bundle, key);
		}

		public static TParameter GetObject<TParameter>(this Bundle bundle, string key)
			where TParameter : class, new()
		{
			var bytes = bundle.GetByteArray(key);
			return Deserialize<TParameter>(bytes);
		}

		private static TParameter Deserialize<TParameter>(byte[] bytes)
			where TParameter : class, new()
		{
			if (bytes == null)
			{
				return default(TParameter);
			}

			var formatter = new BinaryFormatter();
			var stream = new MemoryStream(bytes);

			return formatter.Deserialize(stream) as TParameter;
		}

		private static byte[] Serialize(object obj)
		{
			var formatter = new BinaryFormatter();
			var stream = new MemoryStream();

			formatter.Serialize(stream, obj);

			return stream.ToArray();
		}
    }
}