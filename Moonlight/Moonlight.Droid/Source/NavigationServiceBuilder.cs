using System;

namespace Moonlight.Droid
{
    public class NavigationServiceBuilder
    {
        private readonly NavigationService service;

        public NavigationServiceBuilder(string parameterExtraKey)
        {
            service = new NavigationService(parameterExtraKey);
        }

        public NavigationServiceBuilder AddPage(string key, Type activityType)
        {
            service.activityMap.Add(key, activityType);
            return this;
        }

        public INavigationService Build()
        {
            return service;
        }
    }
}