﻿using System;
using Xunit;

namespace Vivaldi.Test
{
    public class SeasonTest
    {
        Season target;

        [Fact]
        public void CanTellItContainsDateTime()
        {
            WithSpring2017();

            var someDay = new DateTime(2017, 4, 20);
            Assert.True(target.Contains(someDay));
        }

        [Fact]
        public void CanTellItDoesntContainDateTime()
        {
            WithSpring2017();

            var someDay = new DateTime(2016, 4, 20);
            Assert.False(target.Contains(someDay));
        }

        void WithSpring2017()
        {
            // A hypothetical spring in Belgium
            var start = new DateTime(2017, 3, 21);
            var end = new DateTime(2017, 6, 20);

            target = new Season(Season.Name.Spring, start, end);
        }
    }
}
