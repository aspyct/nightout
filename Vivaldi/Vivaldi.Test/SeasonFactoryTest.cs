﻿using System;
using Xunit;

namespace Vivaldi.Test
{
    public class SeasonFactoryTest
    {
        SeasonFactory north;

        [Fact]
        public void CreatesSeasonsFromDateTime()
        {
            WithNorthernHemisphere();

            var now = DateTime.Now;
            var season = north.Create(now);
        }

        [Fact]
        public void SpringStartsMarch21()
        {
            WithNorthernHemisphere();

            var date = new DateTime(2017, 3, 21);
            var season = north.Create(date);

            Assert.Equal(Season.Name.Spring, season.Which);
            Assert.Equal(2017, season.Start.Year);
            Assert.Equal(3, season.Start.Month);
            Assert.Equal(21, season.Start.Day);
        }

        [Fact]
        public void SpringEndsJune20()
        {
            WithNorthernHemisphere();

            var date = new DateTime(2017, 6, 20);
            var season = north.Create(date);

            Assert.Equal(Season.Name.Spring, season.Which);
            Assert.Equal(2017, season.Start.Year);
            Assert.Equal(6, season.End.Month);
            Assert.Equal(20, season.End.Day);
        }

        [Fact]
        public void SummerStartsJune21()
        {
            WithNorthernHemisphere();

            var date = new DateTime(2017, 6, 21);
            var season = north.Create(date);

            Assert.Equal(Season.Name.Summer, season.Which);
            Assert.Equal(2017, season.Start.Year);
            Assert.Equal(6, season.Start.Month);
            Assert.Equal(21, season.Start.Day);
        }

        [Fact]
        public void SummerEndsSeptember20()
        {
            WithNorthernHemisphere();

            var date = new DateTime(2017, 9, 20);
            var season = north.Create(date);

            Assert.Equal(Season.Name.Summer, season.Which);
            Assert.Equal(2017, season.End.Year);
            Assert.Equal(9, season.End.Month);
            Assert.Equal(20, season.End.Day);
        }

        [Fact]
        public void AutumnStartsSeptember21()
        {
            WithNorthernHemisphere();

            var date = new DateTime(2017, 9, 21);
            var season = north.Create(date);

            Assert.Equal(Season.Name.Autumn, season.Which);
            Assert.Equal(2017, season.Start.Year);
            Assert.Equal(9, season.Start.Month);
            Assert.Equal(21, season.Start.Day);
        }

        [Fact]
        public void AutumnEndsDecember20()
        {
            WithNorthernHemisphere();

            var date = new DateTime(2017, 12, 20);
            var season = north.Create(date);

            Assert.Equal(Season.Name.Autumn, season.Which);
            Assert.Equal(2017, season.End.Year);
            Assert.Equal(12, season.End.Month);
            Assert.Equal(20, season.End.Day);
        }

        [Fact]
        public void WinterStartsDecember21()
        {
            WithNorthernHemisphere();

            var date = new DateTime(2017, 12, 21);
            var season = north.Create(date);

            Assert.Equal(Season.Name.Winter, season.Which);
            Assert.Equal(2017, season.Start.Year);
            Assert.Equal(12, season.Start.Month);
            Assert.Equal(21, season.Start.Day);
        }

        [Fact]
        public void WinterEndsMarch20()
        {
            WithNorthernHemisphere();

            var date = new DateTime(2018, 3, 20);
            var season = north.Create(date);

            Assert.Equal(Season.Name.Winter, season.Which);
            Assert.Equal(2018, season.End.Year);
            Assert.Equal(3, season.End.Month);
            Assert.Equal(20, season.End.Day);
        }

        [Fact]
        public void WinterAtEndOfYearHopsToNextYear()
        {
            WithNorthernHemisphere();

            var date = new DateTime(2017, 12, 25);
            var season = north.Create(date);

            Assert.Equal(2017, season.Start.Year);
            Assert.Equal(2018, season.End.Year);
        }

        [Fact]
        public void WinterAtStartOfYearLooksBack()
        {
            WithNorthernHemisphere();

            var date = new DateTime(2018, 1, 4);
            var season = north.Create(date);

            Assert.Equal(2017, season.Start.Year);
            Assert.Equal(2018, season.End.Year);
        }

        [Fact]
        public void SwappedAroundInSouthernHemipshere()
        {
            var south = new SeasonFactory(SeasonFactory.Hemisphere.South);

            // would be spring in northern hemisphere
            var date = new DateTime(2017, 4, 21);
            var season = south.Create(date);

            Assert.Equal(Season.Name.Autumn, season.Which);
        }

        void WithNorthernHemisphere()
        {
            north = new SeasonFactory(SeasonFactory.Hemisphere.North);
        }
    }
}
