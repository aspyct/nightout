﻿using System;
namespace Vivaldi
{
    public class Season
    {
        public readonly Name Which;
        public readonly DateTime Start;
        public readonly DateTime End;

        internal Season(Name which, DateTime start, DateTime end)
        {
            Which = which;
            Start = start;
            End = end;
        }

        public bool Contains(DateTime someDay)
        {
            return Start <= someDay && someDay <= End;
        }

        public enum Name
        {
            Spring,
            Summer,
            Autumn,
            Winter
        }
    }
}
