﻿using System;
namespace Vivaldi
{
    public interface ISeasonFactory
    {
        Season Create(DateTime date);
    }
}
