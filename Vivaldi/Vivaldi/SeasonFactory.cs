﻿using System;
namespace Vivaldi
{
    public class SeasonFactory : ISeasonFactory
    {
        readonly Hemisphere hemisphere;

        public SeasonFactory(Hemisphere hemisphere = Hemisphere.North)
        {
            this.hemisphere = hemisphere;
        }

        public Season Create(DateTime date)
        {
            int value = date.Month * 100 + date.Day;

            if (value < 321)
            {
                return new Season(
                    SwapIfNeeded(Season.Name.Winter),
                    new DateTime(date.Year - 1, 12, 21),
                    new DateTime(date.Year, 3, 20)
                );
            }

            if (value < 621)
            {
                return new Season(
                    SwapIfNeeded(Season.Name.Spring),
                    new DateTime(date.Year, 3, 21),
                    new DateTime(date.Year, 6, 20)
                );
            }

            if (value < 921)
            {
                return new Season(
                    SwapIfNeeded(Season.Name.Summer),
                    new DateTime(date.Year, 6, 21),
                    new DateTime(date.Year, 9, 20)
                );
            }

            if (value < 1221)
            {
                return new Season(
                    SwapIfNeeded(Season.Name.Autumn),
                    new DateTime(date.Year, 9, 21),
                    new DateTime(date.Year, 12, 20)
                );
            }

            return new Season(
                SwapIfNeeded(Season.Name.Winter),
                new DateTime(date.Year, 12, 21),
                new DateTime(date.Year + 1, 3, 20)
            );
        }

        Season.Name SwapIfNeeded(Season.Name northernSeason)
        {
            if (hemisphere == Hemisphere.North)
            {
                return northernSeason;
            }

            switch (northernSeason)
            {
                case Season.Name.Spring: return Season.Name.Autumn;
                case Season.Name.Summer: return Season.Name.Winter;
                case Season.Name.Autumn: return Season.Name.Spring;
                case Season.Name.Winter: return Season.Name.Summer;
            }

            throw new InvalidOperationException($"Unknown season: {northernSeason}");
        }

        public enum Hemisphere
        {
            North,
            South
        }
    }
}
