using Xunit;

namespace Monad.Test
{
    public class MaybeTest
    {
        [Fact]
        public void WrapsNullIntoNothing()
        {
			var maybe = Maybe.Return(default(string));
			Assert.IsType<Nothing<string>>(maybe);
        }

		[Fact]
		public void WrapsObjectIntoJust()
		{
			var expected = "";
			var maybe = Maybe.Return(expected);

			Assert.IsType<Just<string>>(maybe);

			var just = (Just<string>)maybe;
			Assert.Equal(expected, just.Value);
		}

		[Fact]
		public void WrapsZeroIntoJust()
		{
			var expected = 0;
			var maybe = Maybe.Return(expected);

			Assert.IsType<Just<int>>(maybe);

			var just = (Just<int>)maybe;
			Assert.Equal(expected, just.Value);
		}
    }
}
