using System;
using Xunit;

namespace Monad.Test
{
	public class JustTest
	{
		[Fact]
		public void OrReturnsJustValue()
		{
			var expected = true;
			var just = Maybe.Return(expected);

			var result = just.Or(!expected);
			Assert.Equal(expected, result);
		}

		[Fact]
		public void OrOverloadReturnsJustValue()
		{
			var expected = true;
			var just = Maybe.Return(expected);

			var result = just.Or(() => !expected);
			Assert.Equal(expected, result);
		}

		[Fact]
		public void CaseUsesJustFunction()
		{
			var just = Maybe.Return("hello");

			bool called = false;
			var result = just.Case((_) =>
			{
				called = true;
				return "world";
			}, () =>
			{
				Assert.True(false, "Shouldn't call this function");
				return "";
			});

			Assert.True(called);
			Assert.Equal("world", result);
		}

		[Fact]
		public void CaseOverloadUsesJustFunction()
		{
			var just = Maybe.Return(true);

			bool called = false;
			var result = just.Case((_) =>
			{
				called = true;
				return 42;
			}, 0);

			Assert.True(called);
			Assert.Equal(42, result);
		}

		[Fact]
		public void SelectUsesFunction()
		{
			var value = 42;
			var expected = "hello";
			var just = Maybe.Return(value);

			bool called = false;
			var result = just.Select(v =>
			{
				called = true;
				Assert.Equal(value, v);
				return expected;
			});

			Assert.True(called);
			Assert.IsType<Just<string>>(result);

			var mapped = (Just<string>)result;
			Assert.Equal(expected, mapped.Value);
		}

		[Fact]
		public void SelectTurnsNullIntoNothing()
		{
			var just = Maybe.Return(true);
			var result = just.Select(_ => default(string));

			Assert.IsType<Nothing<string>>(result);
		}

		[Fact]
		public void DoUsesJustAction()
		{
			var expected = 42;
			var just = Maybe.Return(expected);

			bool called = false;
			var maybe = just.Do(
				just: val =>
				{
					Assert.Equal(expected, val);
					called = true;
				},
				nothing: () =>
				{
					Assert.True(false, "Shouldn't call this function");
				}
			);

			Assert.True(called);
			Assert.Same(just, maybe);
		}

        [Fact]
        public void JustHasValue()
        {
            var just = Maybe.Return(42);
            Assert.True(just.HasValue);
        }
	}
}
