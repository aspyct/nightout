﻿using System;
using Xunit;

namespace Monad.Test
{
    public class NothingTest
    {
        [Fact]
        public void OrReturnsNothingValue()
        {
            var nothing = Nothing<string>.Default;
            var expected = "hello";

            var result = nothing.Or(expected);
            Assert.Equal(expected, result);
        }

        [Fact]
        public void OrOverloadUsesNothingFunction()
        {
            var nothing = Nothing<string>.Default;
            var expected = "hello";

            var called = false;
            var result = nothing.Or(() =>
            {
                called = true;
                return expected;
            });

            Assert.True(called);
            Assert.Equal(expected, result);
        }

        [Fact]
        public void CaseUsesNothingFunction()
        {
            var nothing = Nothing<string>.Default;
            var expected = "hello";

            var called = false;
            var result = nothing.Case(
                just: (_) =>
                {
                    return "nope";
                },
                nothing: () =>
                {
                    called = true;
                    return expected;
                }
            );

            Assert.True(called);
            Assert.Equal(expected, result);
        }

        [Fact]
        public void CaseOverloadReturnsNothingValue()
        {
            var nothing = Nothing<string>.Default;
            var expected = "hello";

            var miscalled = false;
            var result = nothing.Case((_) =>
            {
                miscalled = true;
                return "nope";
            }, expected);

            Assert.False(miscalled);
            Assert.Equal(expected, result);
        }

        [Fact]
        public void SelectReturnsNothingIfNoFunctionProvided()
        {
            var nothing = Nothing<string>.Default;

            var result = nothing.Select(_ => 42);

            Assert.IsType<Nothing<int>>(result);
        }

        [Fact]
        public void SelectCallsProvidedFunction()
        {
            var expected = 42;
            var nothing = Nothing<string>.Default;

            bool called = false;
            var result = nothing.Select(
                just: _ =>
                {
                    Assert.True(false, "Shouldn't be called");
                    return 0;
                },
                nothing: () =>
                {
                    called = true;
                    return expected;
                }
            );

            Assert.True(called);
            Assert.IsType<Just<int>>(result);

            var just = (Just<int>)result;
            Assert.Equal(expected, just.Value);
        }

        [Fact]
        public void SelectReturnsPovidedValue()
        {
            var expected = "hello";

            var nothing = Nothing<string>.Default;
            var result = nothing.Select(just: _ => "world",
                                        nothing: expected);

            Assert.IsType<Just<string>>(result);

            var just = (Just<string>)result;
            Assert.Equal(expected, just.Value);
        }

        [Fact]
        public void SelectReturnsNothingIfFunctionReturnsNull()
        {
            var nothing = Nothing<string>.Default;
            var result = nothing.Select(just: _ => "hello",
                                        nothing: () => null);

            Assert.IsType<Nothing<string>>(result);
        }

        [Fact]
        public void SelectReturnsNothingIfValueIsNull()
        {
            var nothing = Nothing<string>.Default;
            var result = nothing.Select(just: _ => "hello",
                                        nothing: default(string));

            Assert.IsType<Nothing<string>>(result);
        }

        [Fact]
        public void DoUsesNothingFunction()
        {
            var nothing = Nothing<string>.Default;

            var called = false;
            var maybe = nothing.Do(
                just: _ =>
                {
                    Assert.True(false, "Shouldn't call this function");
                },
                nothing: () =>
                {
                    called = true;
                }
            );

            Assert.True(called);
            Assert.Same(nothing, maybe);
        }

        [Fact]
        public void DoAcceptsNullFunction()
        {
            var nothing = Nothing<string>.Default;
            nothing.Do(just: _ => Assert.True(false, "Shouldn't call this function"));

            Assert.True(true, "Didn't throw an exception.");
        }

        [Fact]
        public void NothingHasNoValue()
        {
            var nothing = Nothing<int>.Default;
            Assert.False(nothing.HasValue);
        }
    }
}
