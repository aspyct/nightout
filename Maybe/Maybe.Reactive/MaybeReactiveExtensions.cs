﻿using System;
using System.Reactive.Linq;
using Monad;

namespace Monad.Reactive
{
    public static class MaybeReactiveExtensions
    {
        public static IObservable<Maybe<TReturn>> MaybeSelect<TValue, TReturn>(
            this IObservable<Maybe<TValue>> self,
            Func<TValue, TReturn> just,
            Func<TReturn> nothing = null
        )
        {
            return self.Select(maybe => maybe.Select(just, nothing));
        }

        public static IObservable<Maybe<TReturn>> MaybeSelect<TValue, TReturn>(
            this IObservable<Maybe<TValue>> self,
            Func<TValue, TReturn> just,
            TReturn nothing
        )
        {
            return self.Select(maybe => maybe.Select(just, nothing));
        }

        public static IObservable<bool> HasValue<TValue>(
            this IObservable<Maybe<TValue>> self
        )
        {
            return self.Select(maybe => maybe.HasValue);
        }
    }
}
