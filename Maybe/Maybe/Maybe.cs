using System;

namespace Monad
{
    public static class Maybe
    {
        public static Maybe<TValue> Return<TValue>(TValue value)
        {
            // ValueTypes will never be == null, so please ignore this warning.
            if (value == null)
            {
                // Null is nothing!
                return Nothing<TValue>.Default;
            }
            else
            {
                return new Just<TValue>(value);
            }
        }
    }

    [Serializable]
    public abstract class Maybe<TValue>
    {
        public abstract TReturn Case<TReturn>(Func<TValue, TReturn> just, Func<TReturn> nothing);
        public abstract TReturn Case<TReturn>(Func<TValue, TReturn> just, TReturn nothing);

        public abstract TValue Or(Func<TValue> nothing);
        public abstract TValue Or(TValue nothing);

        /// <summary>
        /// Transforms the value of this Maybe with <paramref name="just"/>.
        /// Nothing maps to Nothing.
        /// Just maps to either another Just, or Nothing if the map result is null.
        /// </summary>
        /// <returns>Either Just or Nothing</returns>
        /// <param name="just">The function to apply to the value</param>
        /// <typeparam name="TReturn">The output type of the <paramref name="just"/> function</typeparam>
        public abstract Maybe<TReturn> Select<TReturn>(Func<TValue, TReturn> just, Func<TReturn> nothing = null);

        public abstract Maybe<TReturn> Select<TReturn>(Func<TValue, TReturn> just, TReturn nothing);

        /// <summary>
        /// This method is intended to perform side effects.
        /// </summary>
        /// <returns>This maybe, unmodified</returns>
        /// <param name="just">Action to do if this is a Just</param>
        /// <param name="nothing">Action to do if this is a Nothing</param>
        public abstract Maybe<TValue> Do(Action<TValue> just, Action nothing = null);

        public abstract bool HasValue { get; }
    }
}
