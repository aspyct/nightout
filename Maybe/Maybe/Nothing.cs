using System;

namespace Monad
{
    [Serializable]
    public class Nothing<TValue> : Maybe<TValue>
    {
        public static Maybe<TValue> Default = new Nothing<TValue>();

        public override TReturn Case<TReturn>(Func<TValue, TReturn> just, TReturn nothing)
        {
            return nothing;
        }

        public override TReturn Case<TReturn>(Func<TValue, TReturn> just, Func<TReturn> nothing)
        {
            return nothing();
        }

        public override TValue Or(Func<TValue> nothing)
        {
            return nothing();
        }

        public override TValue Or(TValue nothing)
        {
            return nothing;
        }

        public override Maybe<TReturn> Select<TReturn>(Func<TValue, TReturn> just, Func<TReturn> nothing = null)
        {
            if (nothing == null)
            {
                return Nothing<TReturn>.Default;
            }
            else
            {
                var mapped = nothing();
                return Select(just, mapped);
            }
        }

        public override Maybe<TReturn> Select<TReturn>(Func<TValue, TReturn> just, TReturn nothing)
        {
            return Maybe.Return(nothing);
        }

        public override Maybe<TValue> Do(Action<TValue> just, Action nothing = null)
        {
            if (nothing != null)
            {
                nothing();
            }

            return this;
        }

        public override bool HasValue => false;
    }
}
