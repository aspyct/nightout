using System;

namespace Monad
{
    [Serializable]
    public class Just<TValue> : Maybe<TValue>
    {
        public TValue Value { get; private set; }

        public Just()
        {

        }

        internal Just(TValue value)
        {
            Value = value;
        }

        public override TReturn Case<TReturn>(Func<TValue, TReturn> just, TReturn nothing)
        {
            return just(Value);
        }

        public override TReturn Case<TReturn>(Func<TValue, TReturn> just, Func<TReturn> nothing)
        {
            return just(Value);
        }

        public override TValue Or(Func<TValue> nothing)
        {
            return Value;
        }

        public override TValue Or(TValue nothing)
        {
            return Value;
        }

        public override Maybe<TReturn> Select<TReturn>(Func<TValue, TReturn> just, Func<TReturn> nothing = null)
        {
            return Select(just, default(TReturn));
        }

        public override Maybe<TReturn> Select<TReturn>(Func<TValue, TReturn> just, TReturn nothing)
        {
            var mapped = just(Value);
            return Maybe.Return(mapped);
        }

        public override Maybe<TValue> Do(Action<TValue> just, Action nothing = null)
        {
            just(Value);

            return this;
        }

        public override bool HasValue => true;
    }
}
