﻿namespace System
{
    /// <summary>
    /// This is a fake [Serializable] attribute,
    /// to lure the system into serializing stuff.
    /// Seems to work like a charm.
    /// (The default [Serializable] attr isn't available in a NetStandard library)
    /// </summary>
    public class SerializableAttribute : Attribute
    {
    }
}
